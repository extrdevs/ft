var rek = require('rekuire');
var CONFIG = rek('data-config');

var CITIES = [

    {
        name: "Pune",
        short_name : "Pune",
        type: CONFIG.AREA_TYPE[3]._id,
        city:"Pune",
        state: "Maharashtra",
        country: "India",
        lat:"18.5220577",
        lng:"73.8564007",
    },
    {
        name: "Mumbai",
        short_name : "Mumbai",
        type: CONFIG.AREA_TYPE[3]._id,
        city:"Mumbai",
        state: "Maharashtra",
        country: "India",
        lat:"19.0756595",
        lng:"72.8783426",
    },
    {
        name: "Nagpur",
        short_name : "nagpur",
        type: CONFIG.AREA_TYPE[3]._id,
        city:"Nagpur",
        state: "Maharashtra",
        country: "India",
        lat:"21.1474010",
        lng:"79.0881550",
    }
];

module.exports = CITIES;
