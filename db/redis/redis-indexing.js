//***** Redis auto complete initialization*****
var rek = require("rekuire");
var AreaModel = rek("areaModel");
var CONFIG = rek("config");
var options = {};
var indexing = {};
var em = rek("event-manager");
var redis = rek("redis-connection");
var logger = rek("logger");

logger.info("redis-indexing script started");
//iterate over all area collection and build redis index
AreaModel.find().lean().exec(function(err, areas) {

    logger.info("Connecting to redis");
    logger.info("HOST: " + CONFIG.DB.REDIS.HOST);
    logger.info("PORT: " + CONFIG.DB.REDIS.PORT);
    logger.info("PASSWORD: " + CONFIG.DB.REDIS.PASSWORD);
    logger.info("COMPLETER_NAMESPACE: " + CONFIG.DB.REDIS.COMPLETER_NAMESPACE);

 //   em.ee.emit("redis-all-done");

/*    var RedisComplete = require("rediscomplete");
    completer = new RedisComplete({
        host: CONFIG.DB.REDIS.HOST,
        port: CONFIG.DB.REDIS.PORT,
        app: CONFIG.DB.REDIS.COMPLETER_NAMESPACE,
        auth: CONFIG.DB.REDIS.PASSWORD
    });*/

    /*if (areas.length > 0) {

        completer.index({
            data: areas,
            complKey: "name",
            idField: "_id",
            ns: "area"
        }, function(err) {
            completer.search({
                search: "pun",
                ns: "area"
            }, function(err, results) {
                if (results.length > 0) {
                    logger.info("redis-indexing done");
                    em.ee.emit("redis-all-done");
                }

            });
        });

    } else {
        logger.info("There no area data for indexing");
        em.ee.emit("redis-all-done");
    }*/

    //exporting this to use in other modules
    //indexing.completer = completer;
});

module.exports = indexing;
