//***** Redis connection setup*****
var rek = require("rekuire");
var CONFIG = require("../../config/config");
var logger = rek("logger");
var em = rek("event-manager");

var options = {};

module.exports = function(callback) {

    if (CONFIG.DB.REDIS.PASSWORD) {
        options = {
            auth_pass: CONFIG.DB.REDIS.PASSWORD
        };
    }

    var redisClient = require("redis").createClient(
        CONFIG.DB.REDIS.PORT,
        CONFIG.DB.REDIS.HOST,
        options
    );

    redisClient.on("error", function(err) {
        logger.error(err);
    }).on("connect", function() {
        logger.info(" [Bootstrap] Redis connection opened");
        callback(redisClient);
    });
};
