//***** MongoDB connection setup*****
var rek = require("rekuire");
var CONFIG = rek("config");
var mongoose = require("mongoose");
var autoInc = require("mongoose-auto-increment");
var logger = rek("logger");
var em = rek("event-manager");

/**
 * Make connection to Mongo DataBase
 * @return mongoose.connection object.
 */
module.exports = function() {
    var mongourl = getConnectionURL();
    logger.info(" [Bootstrap] Connecting mongo db at: " + mongourl);
    mongoose.connect(mongourl);
    autoInc.initialize(mongoose.connection);
    mongoose.connection.on("error", console.error);
    mongoose.connection.once("open", function() {
        logger.info(" [Bootstrap] MongoDB Connection Opened");
        em.ee.emit("mongo-all-done");
        //mongoose-auto-increament start increamenting fom previous index (_id) number
        //hence this is required to start from 0
        /*if (CONFIG.DB.MONGO.IS_REMOVE_AUTO_INC_COUNTER) {
            mongoose.connection.db.dropCollection("identitycounters", function(err, result) {
                if (err) {
                    logger.error(" [Bootstrap] Auto Increament Couter Removal Failed," +
                        "identities will not start from 0", err);
                } else {
                    logger.info(" [Bootstrap] Auto Increament Couter Removal done, couter starts at 0");
                    em.ee.emit("mongo-all-done");
                    //rek("mongo-init-data");
                }
            });
        } else {
            logger.info(" [Bootstrap] Skipped Auto Increament Couter Removal");
            //rek("mongo-init-data");
        }
      */
    });

    // on connection disconnect
    mongoose.connection.on("disconnected", function() {
        logger.info("Mongoose default connection disconnected");
    });
    // on node process end, close the mongoose connection
    process.on("SIGINT", function() {
        mongoose.connection.close(function() {
            logger.log("Mongoose default connection disconnected through app termination");
            process.exit(0);
        });
    });
    return mongoose.connection;
};

/**
 * Create DB connection URL
 * @return URL String
 */
var getConnectionURL = function() {
    var mongourl = "mongodb://" + CONFIG.DB.MONGO.HOST + ":" + CONFIG.DB.MONGO.PORT + "/" + CONFIG.DB.MONGO.DB_NAME;

    if (CONFIG.DB.MONGO.USERNAME) {
        mongourl = "mongodb://" + CONFIG.DB.MONGO.USERNAME + ":" + CONFIG.DB.MONGO.PASSWORD +
            "@" + CONFIG.DB.MONGO.HOST + ":" + CONFIG.DB.MONGO.PORT + "/" + CONFIG.DB.MONGO.DB_NAME;
    }
    return mongourl;
}
