/*
var rek = require("rekuire");
var mongoose = require("mongoose");
var em = rek("event-manager");
var AreaModel = rek("areaModel");
var async = require("async");
var logger = rek("logger");

var AREA_COLLECTION = "areas";
var collectionsToRecreate = [];
var removals = 0;
var created = 0;
//var dropCounter = true;

//Check if any collection has to be populated and do so if not.
async.series({
    //This will check if area colleciton is already populated.
    areaCollectionCheck: function(callback) {
        logger.info("areaCollectionCheck....");
        var query = AreaModel.find({}).limit(1);
        query.exec(function(err, area) {
            if (area.length == 0) { //no record in area collection
                collectionsToRecreate.push(AREA_COLLECTION);
            }
            callback();
        });
    },

    // Finally recreate collections if they are not already created.
    recreateCollections: function(callback) {
        if (collectionsToRecreate.length == 0) {
            logger.info("No Collecion is available to recreate");
            em.ee.emit("mongo-all-done");
        } else {
            for (var i = 0; i < collectionsToRecreate.length; i++) {
                logger.info("Collection To be removed ->>> ", collectionsToRecreate[i]);
                mongoose.connection.collections[collectionsToRecreate[i]].drop(function(err, result) {
                    if (result) {
                        logger.info("collection removed");
                    } else {
                        logger.info("collection removal failed.");
                    }
                    em.ee.emit("mongo-collection-removed");
                });
            }
        }
        callback();
    }
});

em.ee.on("mongo-collection-removed", function() {

    removals++;

    //once collections to remove done start populating
    //new static data to these collection
    if (removals == collectionsToRecreate.length) {

        for (var i = 0; i < collectionsToRecreate.length; i++) {

            if (collectionsToRecreate[i] == AREA_COLLECTION) {

                var areas = [];
                var countries = rek("countries");
                var states = rek("states");
                var cities = rek("cities");
                var localities = rek("localities");

                areas = countries.concat(states);
                areas = areas.concat(cities);
                areas = areas.concat(localities);

                for (var i = 0, j = 0; i < areas.length; i++) {
                    area = new AreaModel(areas[i]);
                    area.save(function(err, a) {
                        if (err) {
                            logger.error("Error while saving area document", err);
                        } else {
                            logger.info("Area Added:" + a.name);
                            j++;
                            if (j == areas.length) {
                                em.ee.emit("mongo-init-data");
                            }
                        }
                    });
                }

            } else {
                em.ee.emit("mongo-all-done");
            }
        };
    }
});

em.ee.on("mongo-init-data", function() {

    created++;
    if (created == collectionsToRecreate.length) {
        logger.info("mongo-init-data done");
        em.ee.emit("mongo-all-done");
    }

});
*/
