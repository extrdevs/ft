//utility function for updating ratings(postDeatail DB) 
var updateArray = function(arr, postId, rating){


    for (var rep = 0; rep < arr.length; rep++) 
    {
    	//console.log(arr[rep].postId)
    	if(arr[rep].postId == postId) 
    	{
    		arr[rep].rating = rating;
    		//break
    	}
  	}
  	return arr;
};

//utility function for updating ratings(usaerDetail DB)  
var updateArray2 = function(arr, userId, rating){


    for (var rep = 0; rep < arr.length; rep++) 
    {
    	if(arr[rep].userId+"" == ""+userId) 
    	{
    		arr[rep].rating = rating;
    		//break
    	}
  	}
  	return arr;
};

var isRated = function(arr, userId){
    for (var rep = 0; rep < arr.length; rep++) 
    {
      //console.log(arr[rep].postId)
      if(arr[rep].userId == userId) 
      {
        return arr[rep].rating;
      }
    }
    return "false";
};

//utility function for updating ratings(usaerDetail DB)  
var takeAvg = function(arr){

	var rat = 0.0;
	var num = arr.length;
    for (var rep = 0; rep < num; rep++) 
    {
    	rat = rat + arr[rep].rating;
  	}
  	if(num===0) return 0;
  	else return rat/num;
};

module.exports.updateArray = updateArray;
module.exports.updateArray2 = updateArray2;
module.exports.takeAvg = takeAvg;
module.exports.isRated = isRated;