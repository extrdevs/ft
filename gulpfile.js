/* File: gulpfile.js */
'use strict';
// grab our gulp packages
var gulp  = require('gulp');
var gutil = require('gulp-util');
var jshint = require('gulp-jshint');
var nodemon = require('gulp-nodemon');
var git = require('gulp-git');

// Definign high level tasks
gulp.task('default', ['start']);

var path = {
clientJs: './public/js/**.js',
serverJs: [ './config/**/*.js', 
			'./data/*.js',
			'./db/**/*.js', 
			'./event/*.js', 
			'./js/*.js',
			'./models/*.js',
			'./routes/*.js',
			'./util/*.js',
			'./*.js',
			'./test/*.js']
};

gulp.task('load-ServerConfig-test', function(){

});

gulp.task('get-heroku-config-file', function(){

});

gulp.task('load-server-config-dev', function(){

});

//Defining jshint for client code
gulp.task('jshintClient', function(){
	return gulp.src([].concat(path.clientJs))
	.pipe(jshint())
	.pipe(jshint.reporter('jshint-stylish'));
});

//Defining jshint for server code
gulp.task('jshintServer', function(){
	return gulp.src([].concat(path.serverJs))
	.pipe(jshint())
	.pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('compile', ['jshintClient', 'jshintServer']);

// configure which files to watch and what tasks to use on file changes
gulp.task('watch', function() {
  return gulp.watch([].concat(path.clientJs,path.serverJs), ['jshintClient', 'jshintServer']);
});

//nodemon for starting/restarting node server
gulp.task('start', function () {
  nodemon({
    script: 'app.js',
    ext: 'js',
    task: 'jshintServer',
    env: { 'NODE_ENV': 'dev' }
  });
});

var uglify = require('gulp-uglify');
var concat = require('gulp-concat');

gulp.task('compress', function() {
  return gulp.src('routes/*.js')
    //.pipe(uglify())
    .pipe(concat('all.js'))
    .pipe(gulp.dest('dist'));
});

//Push to remote
gulp.task('push-bitbucket', function(){
/*
  git.addRemote('origin', 'https://extrdevs:eexxttrdevs@bitbucket.org/extrdevs/ft.git', function (err) {
    if (err) throw err;
  });
*/
  git.push('origin', 'master', function (err) {
    if (err) throw err;
  });
});

gulp.task('push-heroku', function(){
/*
  git.addRemote('heroku', 'https://git.heroku.com/placetalkr.git', function (err) {
    if (err) throw err;
  });
*/
  git.push('heroku', 'master', function (err) {
    if (err) throw err;
  });
});

gulp.task('push-openshift', function(){
/*
  git.addRemote('openshift', 'ssh://557ee586e0b8cdb58e0000bc@nodejs-placetalkr.rhcloud.com/~/git/nodejs.git', function (err) {
    if (err) throw err;
  });
*/
  git.push('openshift', 'master', function (err) {
    if (err) throw err;
  });
});
