'use strict';
var rek = require('rekuire');
var logger = rek('logger');
var CONFIG = rek('config');

//a Node.JS wrapper library-port for Google Cloud Messaging for Android
var gcm = require('node-gcm');

var mongoose = require('mongoose');
var UserDetail = mongoose.model('UserDetail');

var util = {};

/**
 * Send push notification to client through Google Cloud Messaging(GCM).
 * node-gcm is used to create message and send it to appropriate device or client.
 * GCM_API_KEY: is the API key (Server) of the project configured on google.
 * [https://console.developers.google.com/]
 * It is set at Credentials page of Google Developers Console.
 * New Credentail -> API Key -> Server Key
 *
 * To make this service work we need to enable 'Google Cloud Messaging for Android' from dashboard of Google
 * Developers Console.
 *
 * For more details read at bottom.
 *
 * @param  deviceToken - Device or Client ids to which message/notification has to be sent.
 * @param  message - to be sent to device.
 */
var push = function(device_tokens, userMessage) {
    logger.info('Push Request received');
    logger.debug('device_tokens:', device_tokens);
    logger.debug('userMessage:', userMessage);

    var retry_times = 2; //the number of times to retry sending the message if it fails

    logger.info('GCM_API_KEY:', CONFIG.GCM_API_KEY);
    var sender = new gcm.Sender(CONFIG.GCM_API_KEY); //create a new sender
    var message = new gcm.Message(); //create a new message

    //Message format -> https://developers.google.com/cloud-messaging/http-server-ref#send-downstream
    message.addData('message', userMessage);
    message.addData('sound', 'notification');

    message.collapseKey = 'testing'; //grouping messages
    message.delayWhileIdle = true; //delay sending while receiving device is offline
    message.timeToLive = 3; //the number of seconds to keep the message on the server if the device is offline

    logger.info('Sending push notification to:', device_tokens);
    sender.send(message, { registrationTokens: device_tokens }, retry_times, function(err, response){
        if(err){
            logger.error('err:', err);
            return err;
        }
        else{
            logger.debug('response:', response);
            return response;
        }
    });

};

/**
 * Send notification to the Author of the post, if some one has commented on his/her question/post.
 * @param {Object Id} postAuthorId: Id of the Author of the original post.
 * @param {Object}   comment: Actual comment given for the post.
 */
util.commentNotification = function(post, comment){
    logger.info('commentNotification...');
    //secondaryId needds to be string to be searchable.
    var secondaryId = String(post.author);
    logger.debug('post.author', post.author);
    //Get UserDetail object as it contain list of clients registered by user.
    UserDetail.findOne({secondaryId: secondaryId}, function (err, userDetail) {
        if (err) {
            logger.error('Error searching user', err);
            return err;
        } else {
            //logger.debug('userDetail:', JSON.stringify(userDetail));
            var deviceTokens = userDetail.clientIds;
            if (typeof deviceTokens !== 'undefined' && deviceTokens.length > 0) {
                var message = {
                    title: 'Your question has got an answer',
                    body: comment.details,
                    postId: post._id
                };
                logger.info('calling push');
                push(deviceTokens, message);
            } else{
                logger.info('No device is registered for the user --> ', userDetail.name);
            }
        }
    });
};

module.exports = util;

//////////////////////////////////////////////
/**
GCM (Google Cloud Messaging) Architecture:

Google GCM Connection Servers: accept downstream messages from your app server and send them to a client app. Also from the client app to your app server. The connection server enqueues and stores the message, and then sends it to the client app

App Server: Implement the HTTP and/or XMPP protocol to communicate with the GCM connection server(s). App servers send downstream messages to a GCM connection server.

Client App: is a GCM-enabled client app. To receive and send GCM messages, this app must register with GCM and get a unique identifier called a registration token.


##Key Concepts

This table summarizes the key terms and concepts involved in GCM. It is divided into these categories:

Components — The entities that play a primary role in GCM: GCM Connection Servers, Client App and App Server

Credentials — The IDs and tokens that are used in GCM to ensure that all parties have been authenticated, and that the message is going to the correct place :

    Sender ID: Project Id (Project number from google dashboard), is used in the registration process to identify an app server that is permitted to send messages to the client app.

    API Key: Saved on the app server that gives the app server authorized access to Google services. Used as password to authenticate.

    Application ID: The client app that is registering to receive messages. Use the package name from the app manifest.

    Registration Token: An ID issued by the GCM connection servers to the client app that allows it to receive messages. Note that registration tokens must be kept secret.

Client App Registration:
    1. Android Device send project id (project number) to GCM server for registration.
    2. GCM server return as registration id to the device.
    3. Registration id sent to Application server.

Notification:
    1. Server sends a message to the GCM server with the registration id of each device to receive a message.
    2. GCM server deliver the message.
*/
