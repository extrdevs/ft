"use strict";
var bcrypt = require("bcrypt-nodejs");
var randtoken = require("rand-token");
var jwt = require("jsonwebtoken");
var privateKeyForForgotPwd = "Zipepwld4sk2981ScseDKLSfidue347flgjhsd923DDKGLDFK#34dDf";
var privateKeyForAuthToken = "newuserprivate";


var generateHash = function generateHash(text, cb) {
    bcrypt.genSalt(10, function(err, salt) {
        bcrypt.hash(text, salt, null, function(err, hash) {
            if (err) {
                return cb(err);
            } else {
                cb(err, hash);
            }
        });
    });
};

var generateVerifierToken = function(){
  return randtoken.generate(16);
};

var generateJWTokenForForgotPwd = function(email, verifierToken){
  return jwt.sign({ email: email, verifierToken: verifierToken }, privateKeyForForgotPwd, {expiresInMinutes: "60"});
};

var decodeForgotPwdToken = function(forgotPwdToken){
      return jwt.verify(forgotPwdToken, privateKeyForForgotPwd);
};

var generateAuthToken = function setAuthToken(name, email){
	return jwt.sign({ name: name, email: email }, privateKeyForAuthToken, {expiresInMinutes: "10080"});
};

module.exports.generateHash = generateHash;
module.exports.generateVerifierToken = generateVerifierToken;
module.exports.generateJWToken = generateJWTokenForForgotPwd;
module.exports.decodeForgotPwdToken = decodeForgotPwdToken;
module.exports.generateJWTokenForForgotPwd = generateJWTokenForForgotPwd;
module.exports.generateAuthToken = generateAuthToken;
