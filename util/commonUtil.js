//common-util
var rek = require("rekuire");
var fs = require("fs");
var ejs = require("ejs");
var config = rek("config");

/**
 * Pre-compile the html with the input data.
 * @param  {string} filePath path of the html file to be precompiled.
 * @param  {JSON} data     Data which has to be added to html
 * @return {html string}   Comipled html
 */
var getEjsCompiledHtml = function(filePath, data) {
    // load a template file, then render it with data
    var ejscompiled = ejs.compile(fs.readFileSync(filePath, "utf8"));
    return ejscompiled(data);
};

var getRandomString = function(stringLength) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < stringLength; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

var getHostUrl = function() {
    var hostUrl = "";
    if (process.env.NODE_ENV == "production") {
        hostUrl = config.PROTOCOL + "://" + config.APP_SERVER.HOST_NAME;
    } else {
        hostUrl = config.PROTOCOL + "://" + config.APP_SERVER.HOST_NAME + ":" + config.APP_SERVER.PORT;
    }
    return hostUrl;
};

module.exports.getEjsCompiledHtml = getEjsCompiledHtml;
module.exports.getRandomString = getRandomString;
module.exports.getHostUrl = getHostUrl;
