//EmailConfig
var nodemailer = require("nodemailer"); //http://www.nodemailer.com/
var commonUtil = require("../util/commonUtil");
var rek = require("rekuire");
var logger = rek("logger");
var Promise = require("bluebird");

// create reusable transporter object using SMTP transport
// NB! No need to recreate the transporter object. You can use
// the same transporter object for all e-mails
var transporter = nodemailer.createTransport({
    service: "Zoho",
    auth: {
        user: "admin@placetalkr.com",
        pass: "aaddmmin"
    }
});

/**
 * Send email after user is registered. This for confirming his/her registration.
 * @param  user json object contaning details
 */
var sendRegistrationEmail = function(user) {

    var validationUrl = user.hostUrl + "/user/verify/" + user.token;

    var registrationEmailTemplatePath = "./views/email/action.html";
    //This object contain dynamic values which need to be included in HTML.
    var compiledHtml = commonUtil.getEjsCompiledHtml(registrationEmailTemplatePath, {
        url: validationUrl
    });
    //logger.debug("HTML: " + compiledHtml);
    // setup e-mail data
    var mailOptions = {
        from: "admin@placetalkr.com", // sender address
        to: user.email,
        subject: "Welcome to PlaceCus", // Subject line
        html: compiledHtml // html body
    };

    return new Promise(function(resolve, reject) {
        transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
                reject(error);
            } else {
                resolve();
            }
        });
    });
};

/**
 * Send email after user is registered. This for confirming his/her registration.
 * @param  user json object contaning details
 */
// var sendPasswordResetEmail = function(user) {
//     var passwordResetEmailTemplatePath = "./views/email/password-reset.html";
//     //This object contain dynamic values which need to be included in HTML.
//     var data = {
//         password: user.password
//     };
//
//     var compiledHtml = commonUtil.getEjsCompiledHtml(passwordResetEmailTemplatePath, data);
//     //logger.debug("HTML: " + compiledHtml);
//     // setup e-mail data
//     var mailOptions = {
//         from: "admin@placetalkr.com", // sender address
//         to: user.email,
//         subject: "PlaceCus password reset", // Subject line
//         html: compiledHtml // html body
//     };
//
//     return new Promise(function(resolve, reject) {
//         transporter.sendMail(mailOptions, function(error, info) {
//             if (error) {
//                 reject(error);
//             } else {
//                 resolve();
//             }
//         });
//     });
//   };

  var sendForgotPasswordEmail = function(user) {

      var validationUrl = user.hostUrl + "/user/fp/verify?t=" + user.token;

      var forgotPwdEmailTemplatePath = "./views/email/forgotPwdEmailTemplate.html";
      //This object contain dynamic values which need to be included in HTML.
      var compiledHtml = commonUtil.getEjsCompiledHtml(forgotPwdEmailTemplatePath, {
          url: validationUrl
      });
      //logger.debug("HTML: " + compiledHtml);
      // setup e-mail data
      var mailOptions = {
          from: "admin@placetalkr.com", // sender address
          to: user.email,
          subject: "Place Account Password Reset", // Subject line
          html: compiledHtml // html body
      };

      return new Promise(function(resolve, reject) {
          transporter.sendMail(mailOptions, function(error, info) {
              if (error) {
                  reject(error);
              } else {
                  resolve();
              }
          });
      });
  };

  var sendPasswordResetDoneEmail = function(user) {

      var forgotPwdEmailTemplatePath = "./views/email/passwordResetConfirmation.html";
      //This object contain dynamic values which need to be included in HTML.
      var compiledHtml = commonUtil.getEjsCompiledHtml(forgotPwdEmailTemplatePath, {});
      //logger.debug("HTML: " + compiledHtml);
      // setup e-mail data
      var mailOptions = {
          from: "admin@placetalkr.com", // sender address
          to: user.email,
          subject: "Password Reset Completed", // Subject line
          html: compiledHtml // html body
      };

      return new Promise(function(resolve, reject) {
          transporter.sendMail(mailOptions, function(error, info) {
              if (error) {
                  reject(error);
              } else {
                  resolve();
              }
          });
      });
  };

var validateEmail = function(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
};

module.exports.transporter = transporter;
module.exports.sendRegistrationEmail = sendRegistrationEmail;
module.exports.sendForgotPasswordEmail = sendForgotPasswordEmail;
module.exports.validateEmail = validateEmail;
module.exports.sendPasswordResetDoneEmail = sendPasswordResetDoneEmail;
