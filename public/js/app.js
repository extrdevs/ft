"use strict";

var app = angular.module("app", []);

app.config(["$locationProvider", function AppConfig($locationProvider) {
    $locationProvider.html5Mode(true);
}]);

app.controller("forgotPwdCtrl", function($scope, $location, $http) {

    $scope.showForm = true;

    $scope.reset = function() {
       if($scope.pwd === undefined || $scope.retypedPwd === undefined){
            alert("Please fill in password");
       } else if ($scope.pwd !== $scope.retypedPwd) {
            alert("Retyped password doesn't match with the password");
        } else {
            if ($scope.pwd.lenth > 8) {
                alert("Password should be minimum of 8 characters");
            } else {
                var params = $location.search();
                var data = {
                    token: params.t,
                    pwd: $scope.pwd
                };
                $scope.isLoading = true;
                $http.post("/user/fp/reset", data).then(
                    function(response) {
                        $scope.result = response;
                        $scope.success = true;
                        $scope.showForm = false;
                        $scope.isLoading = false;
                    },
                    function(errorResponse) {
                        $scope.result = errorResponse;
                        $scope.success = false;
                        $scope.showForm = false;
                        $scope.isLoading = false;
                    }
                );
            }
        }
    };
});
