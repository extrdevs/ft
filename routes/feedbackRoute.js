//TODO Rename filename to something accountServiceRoute
// Route -/user
"use strict";
var passport = require("passport");

var mongoose = require("mongoose");
var Feedback = mongoose.model("Feedback");

var rek = require("rekuire");
var emailUtil = rek("emailUtil");
var logger = rek("logger");
var InsufficientDataError = rek("InsufficientDataError");

var FeedbackRoute = {};

/**
 * Store feedback in feedback data store.
 * @param  rating - It should be number between 1 to 5
 * @param  comments - String of comments maximum of
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
FeedbackRoute.saveFeedback = function(req, res, next) {

    if (!req.body.hasOwnProperty("rating") || !req.body.hasOwnProperty("comments")) {
            var err = new InsufficientDataError("rating or comment is missing");
            next(err)
        }

    passport.authenticate("bearer", function(err, user, info) {

        var feedback = new Feedback();
        feedback.rating = req.body.rating;
        feedback.comments = req.body.comments;

        if(user){
            feedback.author = user._id;
        } else{
            if(req.body.hasOwnProperty("email")){
                var email = req.body.email;
                if(emailUtil.validateEmail(email)){
                    feedback.email = email;
                } else{
                    error.code = 110;
                    error.message = "Please provide email";
                    return res.json(error);
                }

            } else {
                var error = {};
                error.code = 111;
                error.message = "Please provide email";
                return res.json(error);
            }
        }

        feedback.save(function(err) {
            if (err) {
                var error = {};
                error.code = 112;
                error.message = "Error saving in DB";
                return res.json(error);
            } else {
                res.json(feedback);
            }
        });
    })(req, res, next);

}

/////////////////////////////////////////////////////
module.exports = FeedbackRoute;
