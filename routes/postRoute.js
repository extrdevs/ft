// Route -/post
var rek = require("rekuire");
var express = require("express");
var router = express.Router();
var passport = require("passport");
var mongoose = require("mongoose");
var async = require("async");
var logger = rek("logger");
var InsufficientDataError = rek("InsufficientDataError");

var Post = mongoose.model("Post");
var Area = mongoose.model("Area");
var Comments = mongoose.model("Comments");
var UserDetail = mongoose.model("UserDetail");

var pushNotificationUtil = rek("push-notification-util");

var postRoute = {};

/*Get Feedpost Handler*/
/*
This route will give feed items for both latest and older posts.
To get older post than the provided one, set max parameter
To get latest post than the provided one, set since parameter

Service -> Type: GET, End Point: /post/
*/
postRoute.getPosts = function(req, res, next) {
    var reqArea = req.query.area;
    var max = req.query.max;
    var since = req.query.since;
    var count = req.query.count;
    var userId = req.query.userId;

    //now time should be calculated at backend only
	//because client can be in anytime zone and could
	//probably send time not matching backend calculation zone.
	if(max == "now"){
		max = new Date().toISOString();
	}

    if ((max !== undefined) && (since !== undefined)) {
        res.statusCode = 400;
        return res.send("Error 400: Requested for both since and max feed, provide either one them");
    }

    if (count === undefined) {
        count = 20;
    }

    passport.authenticate("bearer", function(err, user, info) {
    //If userId present then return for for the user.
    if(userId){
        logger.debug("userId:", userId);
        async.waterfall([
            function(callback){
                getUserLikedPosts(user, function(err, likedPosts){
                    if(err) callback(err, null);
                    callback(null, likedPosts);
                });
            },
            function(likedPosts, callback){
                getUserPosts(likedPosts, userId, max, since, count, function(err, posts){
                    if(err) callback(err, null);
                    callback(null, posts);
                });
            }
        ], function(err, posts) {
           if(err){
              next(err);
           }else {
              res.json(posts);
           }
        logger.debug("Post route: Waterfall execution done!");
    });
    } else{ //Get the posts of given area.
        if (reqArea === undefined) {
        var error = new Error();
        error.messge = "Place/User Undefined";
        return next(error);
        }
        logger.debug("reqArea:", reqArea);
        async.waterfall([
            function(callback){
                getUserLikedPosts(user, function(err, likedPostsArr){
                    if(err) callback(err, null);
                    callback(null, likedPostsArr);
                });
            },
            function(likedPostsArr, callback){
                getAreaPosts(likedPostsArr, reqArea, max, since, count, function(err, posts){
                    if(err) callback(err, null);
                    callback(null, posts);
                });
            }
        ], function(err, posts) {
           if(err){
              next(err);
           }else {
              res.json(posts);
           }
        logger.debug("Post route: Waterfall execution done!");
        });
        }
    })(req, res, next);
};

/**
 * Get a single post with post id
 * @param {String}   postId
 * Service -> Type: GET, End Point: /post/:postId
 */
postRoute.getPost = function(req, res, next){
    logger.debug("getPost...");
    var postId = req.params.postId;
    logger.debug("postId:", postId);

    passport.authenticate("bearer", function(err, user, info) {
        async.waterfall([
        function(callback){
            getUserLikedPosts(user, function(err, likedPostsArr){
                if(err) callback(err, null);
                callback(null, likedPostsArr);
            });
        },
        function(likedPostsArr, callback){
            Post.findById(postId)
                .populate("author")
                .lean()
                .exec(function(err, post){
                if(err){
                    callback(err, null);
                } else if (post){
                    callback(null, getPostDto(post, likedPostsArr));
                } else {
                    callback(null, null);
                }
            });
        }
        ], function(err, post) {
           if(err){
              next(err);
           }else {
              res.json(post);
           }
        logger.debug("Post route: Waterfall execution done!");
        });
    })(req, res, next);
};

function getPostDto(post, likedPostsArr){
    var found = false;
       //logger.debug(posts[i].rank);
    if (likedPostsArr !== undefined) {
      found = likedPostsArr.some(function(element, index, likedPostsArr) {
        return element.equals(post._id);
        });
    }
    post.islike = found ? "true" : "false";
    post.rating =  post.ratings ? post.ratings.length : 0 ;
    post.created = Date.parse(post.created);
    post.modified = Date.parse(post.modified);
    //logger.debug("post:", JSON.stringify(post);
    return post;
}

function getUserLikedPosts(user, callback){
    //console.log("user:", user);
    if (user !== false) {
        UserDetail.findOne({
                "secondaryId": user._id
            })
            .lean()
            .exec(function(err, useDetail) {
                //console.log("useDetail:", useDetail);
                if(err) callback(err, null);
                if(!useDetail) callback(null, undefined);
                var likedPostsArr = useDetail.likedPosts;
                callback(null, likedPostsArr);
            });
    } else {
        callback(null, undefined);
    }
}

/**
 * Get the posts for a given area.
 * @param   {String}   reqArea Area for which posts are needed.
 * @param   {Integer}  max     max timestamp by which post was created.
 * @param   {Date}     since   how old posts needed.
 * @param   {Integer} count   Number of posts need to be returned.
 * @returns {Array} post array.
 */
function getAreaPosts(likedPostsArr, reqArea, max, since, count, callback){
    var areas = [reqArea];
    var postsDto = [];
    async.each(areas,
        // 2nd param is the function that each item is passed to
        function(area, callback) {
            // call to asynchronous function, often a save() to DB
            logger.debug("area:", area);
            var postQuery = Post.find();
            postQuery.where("area").equals(area);
            if (max) {
                postQuery.where("created").lt(max);
            }
            if (since) {
                postQuery.where("created").gt(since);
            }
            postQuery.limit(count);
            postQuery.sort("-created");
            postQuery.populate("author");
            postQuery.lean();
            postQuery.exec(function(err, posts) {
                if (err) {
                    logger.error(err);
                    callback(err);
                } else {
                    logger.debug("posts.length:", posts.length);
                    for (var i = 0; i < posts.length; i++) {
                        var post = getPostDto(posts[i], likedPostsArr);
                        /*this is added due to weired behavior with since query
                        greater than has return greater than or equal to data by mongoose query in
                        some environment so added this double check*/
                        if(since && post.created <= since)
                            break;
                        postsDto.push(post);
                    }
                    //async annotation, Async call is done, alert via callback
                    callback();
                }
            });
        },
        // 3rd param is the function to call when everything"s done
        function(err) {
            // All tasks are done now
            if (err) {
                logger.debug("error searching though posts");
                callback(err, null);
            } else {
                callback(null, postsDto);
            }
        }
    );
}

/**
 * Get all the post of the user.
 * @param   {String} userId
 * @param   {Integer}  max     max timestamp by which post was created.
 * @param   {Date}     since   how old posts needed.
 * @param   {Integer} count   Number of posts need to be returned.
 * @returns {Array} post array.
 */
function getUserPosts(likedPostsArr, userId, max, since, count, callback){

    UserDetail.findOne({secondaryId: userId}, function(err, userDetail){
        if(err){
            callback(err, null);
        } else if(userDetail){
            //logger.debug("userDetail:", JSON.stringify(userDetail));
            //Find all the posts with list of post ids.
            var postQuery = Post.find({
            "_id": { $in: userDetail.PostedPosts }
            });
            if (max) {
                postQuery.where("created").lt(max);
            }
            if (since) {
                postQuery.where("created").gt(since);
            }
            postQuery.limit(count);
            postQuery.sort("-created");
            postQuery.populate("author");
            postQuery.lean();
            postQuery.exec(function(err, posts){
                if(err){
                    logger.error(err);
                    callback(err, null);
                } else {
                    var postsDto = [];
                    for (var i = 0; i < posts.length; i++) {
                        var post = getPostDto(posts[i], likedPostsArr);
                        /*this is added due to weired behavior with since query
                        greater than has return greater than or equal to data by mongoose query in
                        some environment so added this double check*/
                        if(since && post.created <= since)
                            break;
                        postsDto.push(post);
                    }
                    callback(null, postsDto);
                }
            });
        } else{
            callback(new Error("User does not exist"), null);
        }
    });
}

/**
 * Add Post.
 * @param   {String}   title
 * @param   {String}   Description.

 * Service -> Type: POST, End Point: /post/
 */

postRoute.addPost = function(req, res) {
    logger.debug("Adding new Post");
    //TODO add more validation

    if (!req.body.hasOwnProperty("title") ||
        !req.body.hasOwnProperty("des")) {
        res.statusCode = 400;
        return res.send("Error 400: Post syntax incorrect.");
    }

    var post = new Post({
        title: req.body.title,
        des: req.body.des,
        type: parseInt(req.body.type),
        //area: mongoose.Types.ObjectId(req.body.area),
        area: req.body.area,
        areaName: req.body.areaName,
        image: {
            imgId: req.body.imgId,
            imgVer: undefined
        },
        author: req.user._id,
        likes: 0,
        commentscount: 0,
        likedBy: []
    });

    post.save(function(err, p) {
        if (err) {
            logger.debug("Error while saving post Document");
            res.json(false);
        } else {
            UserDetail.findOne({
                secondaryId: req.user._id,
            }, function(err, cUser) {
                var arr = cUser.PostedPosts;
                arr.push(p._id);
                cUser.PostedPosts = arr;
                cUser.save(function(err) {
                    if (err) {
                        logger.debug("Error while saving UserDetail Document");
                        res.json(false);
                    } else {
                        logger.debug("Post added by user" + cUser.name);
                        res.json(true);
                    }
                });
            });
        }
    });
};

/**
 * Add comment to the post.
 * @param   {String}   postId - Id of the post to which comment has to be added.
 * @param   {object}   Payload - Actual comments.
 * @returns {Object}  Newly added comment.
 *
 * Service -> Type: POST, End Point: /post/:postId/com
 */
postRoute.addComment = function(req, res) {
    logger.info("Adding comments...");
    //TODO change name details to body
    if (!req.body.hasOwnProperty("postId") && !!req.body.hasOwnProperty("payload")) {
        res.statusCode = 400;
        return res.send("Error 400: Post syntax incorrect.");
    }

    Post.findById(req.body.postId, function(err, post) {

        if (err) {
            logger.debug("err");
            res.json(false);
        } else {

            var newComId = post.comLastId + 1;
            Comments.findOne({
                post: req.body.postId,

                //TODO hardcoding to store all comments in single bucket
                //change this later to acommodate in multiple buckets
                page: 1
            }, function(err, commentBucket) {

                var newCount;
                var page;
                if (commentBucket) {
                    newCount = commentBucket.count + 1;
                } else {
                    newCount = 0;
                    page = 1;
                }
                var comment = {
                    _id: newComId,
                    details: req.body.payload,
                    author: req.user._id
                };

                Comments.update({
                    postId: req.body.postId,
                    page: page,
                    count: newCount

                }, {
                    $push: {
                        "comments": comment
                    }

                }, {
                    safe: true,
                    upsert: true
                }, function(err, numberAffected, raw) {
                    if (err) {
                        logger.debug("err");
                        res.json(false);
                    } else {
                        post.comCount = post.comCount + 1;
                        post.comLastId = newComId;
                        post.save(function(err) {
                            if (err) logger.debug("Comment Addition Failed");
                            else {
                                var comResponse = {
                                    id: newComId,
                                    details: comment.details,
                                    author: req.user,
                                    likes: 0,
                                    created: Date.now(),
                                    islike: "false"
                                };
                                logger.debug("Comment Addition Success!");

                                //Send push notification to author of the original post.
                                //if its not belong to him/herself.
                                logger.debug("req.user._id:", req.user._id.toString());
                                logger.debug("post.author:", post.author.toString());
                                if(!req.user._id.equals(post.author)){
                                    pushNotificationUtil.commentNotification(post, comResponse);
                                }

                                res.json(comResponse);
                            }
                        });

                    }
                });
            });
        }
    });
};

/**
 * Get comments for particular post Id
 * @param   {String}   postId - of which comments have to be retrieved.
 * @returns {Array}  commentArray
 * Service -> Type: GET, End Point: /post/:postId/com
 */
postRoute.getComments = function(req, res, next) {

    passport.authenticate("bearer", function(err, user, info) {

        if (user === false) {
            user._id = -1;
        }

        Comments
            .findOne({
                postId: req.params.postId
            })
            .populate("comments.author")
            .lean()
            .exec(function(err, comments) {

                var commentsArray = [];

                if (comments !== null) {
                    for (var i = 0; i < comments.comments.length; i++) {

                        var item = {
                            id: comments.comments[i]._id,
                            details: comments.comments[i].details,
                            author: comments.comments[i].author, //TODO author should be converted to DTO
                            likes: comments.comments[i].likes,
                            created: comments.comments[i].created,
                            islike: (comments.comments[i].likedBy.some(function(likedByUser) {
                                return likedByUser.equals(user._id);
                            }) ? "true" : "false"),

                        };

                        commentsArray.push(item);
                    }
                }
                res.json(commentsArray);

                if (err)
                    return console.error(err);
            });
    })(req, res, next);
};

//get single post for specific id

/* Service -> Type: GET, End Point: /post/:id
postRoute.getPost = function(req, res) {
    var id = req.params.id;

    Post
        .find({
            "_id": mongoose.Types.ObjectId(req.params.id)
        })
        .populate("author")
        .sort({
            date: -1
        })
        .exec(function(err, posts) {
            if (err) {
                return console.error(err);
            } else if (posts.length > 0) {

                var postsDao = [];
                for (var i = 0; i < posts.length; i++) {

                    postsDao.push({
                        "id": posts[i]._id,
                        "title": posts[i].title,
                        "des": posts[i].des,
                        "type": posts[i].type,
                        "place": posts[i].place,
                        "likes": "3",
                        "reviews": "3",
                        "islike": "Like",
                        "image": {
                            "imgId": posts[i].image.imgId,
                            "imgVer": "",
                        }
                    });
                }
                res.json(postsDao);
            } else {
                res.json(id);
            }
            //TODO do not send DAO as is, cast it to DTO to have only required data

        });
};
*/

/**
 * Like/Unlike given post
 * @param {String} true/false (like -> true and unlike -> false)
 * @param {String} userId
 * Service -> Type: PUT, End Point: /post/like
 */
postRoute.likePost = function(req, res, next) {
    var itemId = req.body.itemId;
    var userId = req.user._id;
    var temp = req.body.like;

    var liked = (temp == "true");
    logger.debug("liked: ", liked);

    logger.debug("itemId: ", itemId);
    //Find the Post which is to be liked/unliked
    Post.findById(itemId, function(err, p) {
        if (!p) {
            next(new InsufficientDataError("Item Data not found"));
        } else {
            UserDetail.findOne({
                "secondaryId": userId
            }).exec(function(err, userDetail) {
                    //Get all the posts liked by given user
                    var arr = userDetail.likedPosts;
                    //If there is like request and array does not contain given post item
                    // Or there is unlike request and array does not contain item. Then update item.
                    if ((arr.indexOf(itemId) == -1 && liked === true)
                        || (arr.indexOf(itemId) != -1 && liked === false)) {

                        if (liked) arr.push(mongoose.Types.ObjectId(itemId));
                        else {
                            //remove id from current user"s likedPosts list
                            //list can be replaced with hashset mainly due to this operation
                            var index = arr.indexOf(itemId);
                            if (index > -1) arr.splice(index, 1);
                        }
                        userDetail.likedPosts = arr;
                        userDetail.save(function(err) {
                            if (err) logger.debug("Unable to save userDetail Document");
                            else {
                                //Also update post with the userid who has liked it.
                                var t = p.likedBy;
                                if (liked) {
                                    p.likes = p.likes + 1;
                                    t.push(mongoose.Types.ObjectId(userId));
                                } else {
                                    p.likes = p.likes - 1;
                                    //remove userId from posts
                                    var index = t.indexOf(userId);
                                    if (index > -1) t.splice(index, 1);
                                }
                                p.likedBy = t;
                                p.save(function(err) {
                                    if (err) logger.debug("Unable to save Post Document");
                                    else {
                                        logger.debug("post saved");
                                        res.json({
                                            "result": {
                                                "like": liked
                                            }
                                        });
                                    }
                                });
                            }
                        });

                    } else {//Return what ever is the current status.
                        if (liked === true) {
                            logger.debug("Like == true -> Exists");
                            res.json({
                                        "result": {
                                        "like": true
                                        }
                                    });
                        } else {
                            logger.debug("Like == false -> exists");
                            res.json({
                                        "result": {
                                        "like": false
                                        }
                                    });
                        }
                    }
                });
        }
    });
};

/**
 * Like/Unlike Comment
 * @param   {String}   postId
 * @param   {String}   comId
 * @param   {String}   like flag -> true/false
 * @returns {Object}   result -> {like:}
 * Service -> Type: PUT, End Point: /post/:postId/:comId/like
 */
postRoute.likeUnlikeComment = function(req, res) {

    var postId = req.body.postId;
    var comId = req.body.comId;
    var userId = req.user._id;
    var temp = req.body.like;
    var liked = (temp == "true");
    logger.debug("Request Type: " + (liked === true ? "Like" : "Unlike"));

    //TODO This implementation considers single page currently
    //implement this for multiple pages going forward
    Comments.findOne({
        "postId": postId
    }).exec(function(err, commentsBucket) {

        if (!commentsBucket) {
            logger.debug("Comment not found for PostId:" + postId);
            res.send(400, "Comment not found");
        } else {
            UserDetail.findOne({
                    "secondaryId": userId
                })
                .exec(function(err, userDetail) {

                    var arr = userDetail.likedComments;

                    var foundLiked = false;

                    for (var i = 0, size = arr.length; i < size; i++) {

                        if (arr[i].postId == postId && arr[i].comId == comId) {
                            logger.debug("Like Object found with user");
                            if (liked) {
                                logger.debug("Request Error: Like Request but Like already exist Exists");
                                res.statusCode = 400;
                                return res.send("Error 400: You have already Liked this item");
                            } else {
                                foundLiked = true;
                                arr.splice(i, 1);
                                break;
                            }
                        }
                    }

                    if (!liked && foundLiked === false) {
                        logger.debug("Error: UnlLike Request but Like not exist Exists with User");
                        res.statusCode = 400;
                        return res.send("Error 400: You have already Unliked this item");
                    }

                    if (liked) {
                        arr.push({
                            "postId": mongoose.Types.ObjectId(postId),
                            "comId": comId
                        });
                    }

                    userDetail.likedComments = arr;
                    userDetail.save(function(err) {
                        if (err) {
                            logger.debug("Unable to save userDetail Document");
                        } else {

                            var commentsCount = commentsBucket.count;
                            var comments = commentsBucket.comments;
                            for (var j = 0; j < comments.length; j++) {
                                if (comments[j]._id == comId) {
                                    if (liked) {
                                        commentsBucket.comments[j].likes = commentsBucket.comments[j].likes + 1;
                                        commentsBucket.comments[j].likedBy.push(mongoose.Types.ObjectId(userId));
                                    } else {
                                        commentsBucket.comments[j].likes = commentsBucket.comments[j].likes - 1;
                                        //remove userId from posts
                                        var userIdIndex = commentsBucket.comments[j].likedBy.indexOf(userId);
                                        if (userIdIndex > -1) {
                                            commentsBucket.comments[j].likedBy.splice(userIdIndex, 1);
                                        }
                                    }
                                    commentsBucket.save(function(err) {
                                        if (err) {
                                            logger.debug("Unable to save Comment Document");
                                        } else {
                                            if (liked) {
                                                logger.debug("Comment Like Success!");
                                                res.json({
                                                    "result": {
                                                        "like": "liked"
                                                    }
                                                });
                                            } else {
                                                logger.debug("Comment UnLike Success!");
                                                res.json({
                                                    "result": {
                                                        "like": "unliked"
                                                    }
                                                });
                                            }

                                        }
                                    });
                                }

                            }
                        }
                    });

                });
        }

    });

};

/**
 * Get value like for particular item and user
 * @param {String}   ItemId
 * @returns {Object} result -> {like:}
 * Service -> Type: GET, End Point: /post/:itemId/isLiked
 */
postRoute.isLike = function(req, res) {
    var postId = req.params.itemId;
    var userId = req.user._id;

    Post.findById(postId, function(err, p) {

        if (!p) {

            res.json({
                "result": {
                    "status": "error",
                    "msg": "Server Error"
                }
            });

            logger.debug("Item not Found");

            //return next(new Error("Could not load Document of given Post"));
        } else {
            UserDetail.findOne({
                    "secondaryId": userId
                })
                .exec(function(err, useDetail) {

                    var arr = useDetail.likedPosts;


                    if (arr.indexOf(postId) == -1) {

                        res.json({
                            "result": {
                                "like": false
                            }
                        });
                    } else {

                        res.json({
                            "result": {
                                "like": true
                            }
                        });

                    }
                });
        }
    });
};


//
/**
 * like comment api
 * @param {String} comments Id
 * @param {String} like     (like -> true, unlike -> false)
 * @returns {Object} Post
 * Service -> Type: PUT, End Point: /post/likeComment
 */
postRoute.likeComment = function(req, res) {
    var id = req.body.id;
    var userId = req.user._id;
    var temp = req.body.like;
    var liked = (temp == "true");
    var commentNum = req.body.num;
    logger.debug(id);
    logger.debug(userId);
    logger.debug(temp);
    logger.debug(commentNum);

    Comments.findOne({
        post: mongoose.Types.ObjectId(id)
    }).exec(function(err, p) {
        if (!p) {
            res.send(404, {"msg": "Item Data not found"});
        } else {
            logger.debug(p.comments[commentNum]);
            UserDetail.findOne({
                    "secondaryId": userId
                })
                .exec(function(err, useDetail) {
                    var arr = useDetail.likedComments;
                    if (liked) arr.push(mongoose.Types.ObjectId(id));
                    else {
                        //remove id from current user"s likedPosts list
                        //list can be replaced with hashset mainly due to this operation
                        var index = arr.indexOf(id);
                        if (index > -1) arr.splice(index, 1);
                    }

                    useDetail.likedComments = arr;
                    useDetail.save(function(err) {
                        if (err) logger.debug("Unable to save useDetail Document");
                        else {
                            var t = p.comments[commentNum].likedBy;
                            if (liked) {
                                p.comments[commentNum].meta.likes = p.comments[commentNum].meta.likes + 1;
                                t.push(mongoose.Types.ObjectId(userId));
                            } else {
                                p.comments[commentNum].meta.likes = p.comments[commentNum].meta.likes - 1;
                                var index = t.indexOf(mongoose.Types.ObjectId(userId));
                                if (index > -1) t.splice(index, 1);
                            }
                            p.save(function(err) {
                                if (err) logger.debug("Unable to save comments document");
                                else {
                                    logger.debug("success2");
                                    res.json(p);
                                }
                            });
                        }
                    });
                });
        }
    });
};


module.exports = postRoute;
