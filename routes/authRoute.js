//TODO Rename filename to something accountServiceRoute
//
// Route -/auth
var passport = require("passport");
var jwt = require("jwt-simple");
var tokenSecret = "xayzbcdefglkoqrsstyz";
var rek = require("rekuire");
var logger = rek("logger");
//var cors = require("cors");
//var emailUtil = rek("email-util");
//var commonUtil = rek("commonUtil");
//var securityUtil = rek("securityUtil");
//var Promise = require("bluebird");
//var mongoose = require("mongoose");
//var User = mongoose.model("User");
//var UserVerifier = mongoose.model("UserVerifier");

//var NewUser = mongoose.model("NewUser");

//var router = express.Router();

//var referer = "";

var authRoute = {};

function serverError(res) {
    res.status(500);
    res.send({
        "message": "Internal Server Error"
    });
}


// GET /auth/google/callback
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  If authentication fails, the user will be redirected back to the
//   login page.  Otherwise, the primary route function function will be called,
//   which, in this example, will redirect the user to the home page.
authRoute.googleCallback = function(req, res, next) {
    passport.authenticate("google", function(err, user) {
        if (err) {
            logger.debug("Google Login error >>", err);
            res.render("error.html", {
                err: err
            });
            return;
        }
        logger.debug("Google Login sucess >>");
        //console.log(user);
        var username = user.email.toLowerCase();
        var token = jwt.encode({
            username: username,
            date: Date.now
        }, tokenSecret);
        var userData = {
            token: token,
            user: {
                id: user._id,
                accessToken: user.accessToken,
                name: user.name,
                email: user.email,
                image: {
                    imgId: user.image.imgId,
                    imgVer: user.image.imgVer
                }
            }
        };
        res.render("googleAuth.html", userData);
    })(req, res, next);
};

// GET /auth/facebook/callback
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  If authentication fails, the user will be redirected back to the
//   login page.  Otherwise, the primary route function function will be called,
//   which, in this example, will redirect the user to the home page.
authRoute.facebookCallback = function(req, res, next) {
    passport.authenticate("facebook", function(err, user) {
        if (err) {
            logger.debug("Facebook Login error >>", err);
            res.render("error.html", {
                err: err
            });
            return;
        }
        logger.debug("Facebook Login sucess >>");
        //console.log(user);
        var username = user.email.toLowerCase();
        var token = jwt.encode({
            username: username,
            date: Date.now
        }, tokenSecret);
        var userData = {
            token: token,
            user: {
                id: user._id,
                accessToken: user.accessToken,
                name: user.name,
                email: user.email,
                image: {
                    imgId: user.image.imgId,
                    imgVer: user.image.imgVer
                }
            }
        };
        res.render("googleAuth.html", userData);
    })(req, res, next);
};

authRoute.login = function(req, res, next) {
    passport.authenticate("local", function(err, user, info) {
        if (err) {
          logger.error("[Auth] Passport authentication api failure" + JSON.stringify(err));
          serverError(res);
          //return next(err);
        }
        if (!user) {
          logger.error("[Auth] Authentication failed! User with email *" + user.email + "* is not registered");
            return res.json(401, "Invalid User ID or Password");
        }

        var username = user.email.toLowerCase();
        var token = jwt.encode({
            username: username,
            date: Date.now
        }, tokenSecret);
        //var token = "deepika"
        //fillin token and user DTO
        res.json({
            token: token,
            user: {
                id: user._id,
                name: user.name,
                email: user.email,
                image: {
                    imgId: user.image.imgId,
                    imgVer: user.image.imgVer
                }
            }
        });
        logger.info("[Auth] Login successful for *" + username + "* !");
    })(req, res, next);
};

authRoute.logout = function(req, res) {
    //TODO may want to persist login/logout details for user
    logger.info("Logout Sucess @" + req.user._id);
    res.json(true);
};

/**
Generate random password and send it to registered email id provided.
Password expires after a week. If password is not changed then original password remain intact.
@param email email id of the registered user
//TODO - Set password expiration to 1 day or some hour only.
*/

//Saving or updating new user
// function saveUpdateUser(user) {
//   return new Promise(function(resolve, reject){
//     user.save(function(err, obj) {
//         if (err) {
//             logger.error("[Password Reset] Saving user failed", JSON.stringify(err));
//             reject();
//         } else {
//             logger.debug("[Password Reset] user with email " + user.email + " save success! reset");
//             resolve();
//         }
//     });
//   });
// }

module.exports = authRoute;
