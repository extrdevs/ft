
/*
 * GET home page.
 */
var views_dir = "./views/";

exports.index = function(req, res) {
  //res.render("index", { message: "Hello" });
	res.sendfile(views_dir + "index.html");
};

exports.partials = function(req, res) {
  var filename = req.params.filename;
  if(!filename) return; // might want to change this
  res.sendfile(views_dir + "partials/" + filename + ".html");
  //res.render("partials/" + filename);
};
