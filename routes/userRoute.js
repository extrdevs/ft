//TODO Rename filename to something accountServiceRoute
// Route -/user
"use strict";
var rek = require("rekuire");
var logger = rek("logger");

var mongoose = require("mongoose");
var User = mongoose.model("User");
var NewUser = mongoose.model("NewUser");
var UserDetail = mongoose.model("UserDetail");

var emailUtil = rek("emailUtil");
var InsufficientDataError = rek("InsufficientDataError");
var validator = require("validator");
var Promise = require("bluebird");
var commonUtil = rek("commonUtil");
var securityUtil = rek("securityUtil");
var userRoute = {};

/* SignUp - create new user model
	Pass name, email and password.
*/

function serverError(res) {
    res.status(500);
    res.send({
        "message": "Internal Server Error"
    });
}

//check whether user already registered
function isRegisteredUser(email) {
    return new Promise(function(resolve, reject) {
        User.where("email", email).count(function(err, count) {
            if (err) {
                reject(err);
            } else {
                resolve(count === 0 ? false : true);
            }
        });
    });
}

function hasSignUpStarted(email) {
    return new Promise(function(resolve, reject) {
        NewUser.where("email", email).count(function(err, count) {
            if (err) {
                reject(err);
            } else {
                resolve(count === 0 ? false : true);
            }
        });
    });
}

//user onboarding validation
function validate(req) {

    if (!req.body.hasOwnProperty("name")) {
        logger.error("[User Onboarding] Name Missing");
        return "Name Required";
    } else if (req.body.name.length > 100) {
        logger.error("[User Onboarding] Name length more than valid characters");
        return "Name should should not be more than 100 characters";
    } else if (req.body.name.length < 2) {
        logger.error("[User Onboarding] Name length less than valid characters");
        return "Name should be mimimum of 2 characters";
    }

    if (!req.body.hasOwnProperty("email")) {
        logger.error("[User Onboarding] Email Address Missing");
        return "Email Address Required";
    } else if (!validator.isEmail(req.body.email)) {
        logger.error("[User Onboarding] Invalid Email Address");
        return "Invalid Email Address";
    }

    if (!req.body.hasOwnProperty("pwd")) {
        logger.error("[User Onboarding] Password Missing");
        return "Password Required";
    } else if (req.body.pwd.length > 200) {
        logger.error("[User Onboarding] Password length more than valid characters");
        return "Password should not be more than 200 characters";
    } else if (req.body.pwd.length < 8) {
        logger.error("[User Onboarding] Password length less than valid characters");
        return "Password should be minimum of 8 characters";
    }

    return true;
}

userRoute.addUser = function(req, res) {

    logger.debug("[User Onboarding] ***Started***");

    //var CONFIG = require("../config/config");
    // var protocol = "http";
    // var hostUrl = "";
    //
    // if (process.env.NODE_ENV == "production") {
    //     hostUrl = protocol + "://" + CONFIG.APP_SERVER.HOST_NAME;
    // } else {
    //     hostUrl = protocol + "://" + CONFIG.APP_SERVER.HOST_NAME + ":" + CONFIG.APP_SERVER.PORT;
    // }

    var validationResult = validate(req);
    if (validationResult !== true) {
        res.status(400);
        res.send({
            "message": validationResult
        });
        return;
    }

    return isRegisteredUser(req.body.email).then(function(isUserExists) {
        if (isUserExists) {
            res.status(400);
            return res.send({
                "message": "User with email " + req.body.email + " already registered"
            });
        }

        return hasSignUpStarted(req.body.email).then(function(hasSignUpStarted) {
            if (hasSignUpStarted) {
                res.status(400);
                return res.send({
                    "message": "User with email " + req.body.email + " has already started Sign Up process"
                });
            }

            User.hash(req.body.pwd, function(err, hash) {
                if (err) {
                    logger.error("[User Onboarding] Hashing Error:" + JSON.stringify(err));
                    return serverError(res);
                } else {
                    var newUser = new NewUser();
                    newUser.name = req.body.name;
                    newUser.email = req.body.email.toLowerCase();
                    newUser.pwd = hash;
                    newUser.setToken();
                    newUser.save(function(err, obj) {
                        if (err) {
                            logger.error("[User Onboarding] Saving new user Failed:" + JSON.stringify(err));
                            return serverError(res);
                        } else {
                            logger.info(" [User Onboarding] " + newUser.email + " added to new user list.");
                            var user = {
                                email: newUser.email,
                                hostUrl: commonUtil.getHostUrl(),
                                token: newUser.token
                            };

                            emailUtil.sendRegistrationEmail(user).then(function() {
                                logger.info(" [User Onboarding] Verification email sent to " + user.email);
                                res.status(200);
                                return res.send({
                                    "message": "Email sent to " + user.email + ", please verify to complete Sign Up process"
                                });
                            }).catch(function(error) {
                                logger.error("[User Onboarding] Failed to send email to " + user.email + ":Detaills" + JSON.stringify(error));
                                return serverError(res);
                            });
                        }
                    });
                }
            });

        }).catch(function(error) {
            logger.error("[User Onboarding] hasSignUpStarted() failed :" + JSON.stringify(error));
            return serverError(res);
        });


    }).catch(function(err) {
        logger.error("[User Onboarding] isUserExists() failed :" + JSON.stringify(err));
        return serverError(res);
    });
};


/*
Confirm the user through email link who has just registered himself.
 */
userRoute.verifyNewUserToken = function(req, res) {

    var token = req.params.token;
    logger.debug("[User Onboarding] Confirmation Token received from user: " + token);

    var message = {};
    var success = false;

    try {
        if(!NewUser.isValidToken(token)){
          logger.error("[User Onboarding] Token retrieval error:" + JSON.stringify(err));
          message.title = "Sorry, Registration failed :(";
          message.body = "Verification token is no longer valid, please try to register again";
          return res.render("new-user-confim.html", {
              message: message,
              success: success
          });
        }

        NewUser.findOne({
            token: req.params.token
        }, function(err, newUser) {
            if (err) {
                logger.error("[User Onboarding] Token retrieval error:" + JSON.stringify(err));
                message.title = "Sorry, Registration failed :(";
                message.body = "There is an issue while processing your request. Please try again.";
                res.render("new-user-confim.html", {
                    message: message,
                    success: success
                });
            } else {
                if (newUser !== null) {
                    logger.debug(newUser);
                    var user = new User();
                    user.name = newUser.name;
                    user.email = newUser.email;
                    user.pwd = newUser.pwd;
                    user.save(function(err, obj) {
                        if (err) {
                            logger.debug("Error saving new user confirmation details!");

                            message.title = "Sorry, Registration failed :(";
                            message.body = "There is an issue while processing your request. Please try again.";
                            res.render("new-user-confim.html", {
                                message: message,
                                success: success
                            });
                        } else {
                            //once primary user object is created create secondary user object
                            //check if this will affect the performance while sign up
                            var userDetail = new UserDetail();
                            userDetail.secondaryId = obj.id;
                            userDetail.name = obj.name;
                            userDetail.save(function(err) {
                                if (err) {
                                    logger.error("[User Onboarding] Second User Creation Error!");
                                    message.title = "Sorry, Registration failed :(";
                                    message.body = "There is an issue while processing your request. Please try again.";
                                    res.render("new-user-confim.html", {
                                        message: message,
                                        success: success
                                    });
                                } else {
                                    logger.debug("[User Onboarding] User " + user.email + " created.");
                                    NewUser.remove({
                                        email: user.email
                                    }, function(err) {
                                        if (err) {
                                            logger.error("[User Onboarding] Error removing user entry from new user DB");
                                            message.title = "Sorry, Registration failed :(";
                                            message.body = "There is an issue while processing your request. Please try again.";
                                            res.render("new-user-confim.html", {
                                                message: message,
                                                success: success
                                            });
                                        }
                                        logger.debug("[User Onboarding] User " + user.email + " successfuly removed from newUser collection. Onboarding successfully completed!");
                                        success = true;
                                        message.title = "Congrats! You are now a registered user.";
                                        message.body = "Please login into the app using your credentials.";
                                        res.render("new-user-confim.html", {
                                            message: message,
                                            success: success
                                        });
                                    });
                                }
                            });
                        }
                    });
                } else {
                    logger.error("Could not find user with token " + req.params.token);
                    message.title = "Sorry, Registration failed :(";
                    message.body = "We could not authenticate you. Probably the authentication token provided is not correct. \
					Please try to register again.";
                    res.render("new-user-confim.html", {
                        message: message,
                        success: success
                    });
                }

            }
        });
    } catch (err) {
        logger.error("Error decoding/validating token" + err);
        message.title = "Sorry, Registration failed :(";
        message.body = "We could not authenticate you. Probably the authentication token provided is not correct. \
		Please try to register again.";
        res.render("new-user-confim.html", {
            message: message,
            success: success
        });
    }
};

//Profile Update - Update existing user model
userRoute.updateProfile = function(req, res) {

    User.findById(req.user._id, function(err, user) {
        if (err) {
            res.json(false);
        } else {
            if (req.body.hasOwnProperty("imgId")) {
                logger.debug("User Image Update initiated..");
                user.image.imgId = req.body.imgId;
                updateUser(user);
            } else if (req.body.hasOwnProperty("name")) {
                logger.debug("User name Update initiated..");
                user.name = req.body.name;
                updateUser(user);
            } else if (req.body.hasOwnProperty("email")) {
                //TODO handle email change differently.shuould send mail to confirm
                //email address
                logger.debug("User email Updat initiated..");
                user.email = req.body.email.toLowerCase();
                updateUser(user);
            } else if (req.body.hasOwnProperty("pwd")) {
                logger.debug("User password Update initiated..");
                User.hash(req.body.pwd, function(err, hash) {
                    if (err) {
                        res.send("Error 400: Password Change Error!");
                        res.json(false);
                    } else {
                        user.pwd = hash;
                        updateUser(user);
                    }
                });
            } else {
                //handle error
                res.json(false);
            }
        }

        function updateUser(user) {
            user.save(function(err) {
                if (err) {
                    logger.debug("User Update Failed for " + user.email);
                    logger.debug(err);
                    res.json(false);
                } else {
                    logger.debug("User " + user.email + " updated!");
                    res.json(true);
                }
            });
        }
    });
};

//utility function to get user list(mainly for testing)
userRoute.getAllUsers = function(req, res) {
    User
        .find({})
        .exec(function(err, users) {
            if (err) {
                return console.error(err);
            } else if (users.length > 0) {

                var postsDao = [];
                for (var i = 0; i < users.length; i++) {

                    postsDao.push({
                        "id": users[i].id,
                        "name": users[i].name,
                        "email": users[i].email
                    });
                }
                res.json(postsDao);
            } else {
                res.json({});
            }
        });
};

//utility function to get detailed user list(mainly for testing)
userRoute.getAllUsersDetails = function(req, res) {

    UserDetail
        .find({})
        //.populate("secondaryId")
        .exec(function(err, users) {
            if (err) {
                return console.error(err);
            } else if (users.length > 0) {

                var postsDao = [];
                for (var i = 0; i < users.length; i++) {

                    postsDao.push({
                        "name": users[i].name,
                        "secondaryId": users[i].secondaryId
                    });
                }
                res.json(postsDao);
            } else {
                res.json({});
            }
        });
};

/**
 * Register client e.g. Android phone. (This could be any device on which our application is defined) on
 * the application server. Application server will keep the mapping of clients with user so that notification
 * can be sent to correct user devices.
 * Currently Google Cloud Messaging(GCM) is used for push services.
 * https://developers.google.com/cloud-messaging/gcm
 */
userRoute.registerClient = function(req, res, next) {
    logger.info(" RegisterClient...");
    if (!req.body.hasOwnProperty("deviceToken")) {
        var err = new InsufficientDataError("deviceToken is missing");
        next(err);
    }
    var deviceToken = req.body.deviceToken;
    logger.debug("deviceToken:", deviceToken);
    var secondaryId = String(req.user.id);
    logger.debug("secondaryId:", secondaryId);

    var found = Boolean(false);

    //Retrive user details for storing client registration id i.e client id.
    UserDetail.findOne({
        secondaryId: secondaryId
    }, function(err, userDetail) {
        if (userDetail) {
            //logger.info("userDetail", JSON.stringify(userDetail));
            var clientIds = userDetail.clientIds;
            for (var i = 0; i < clientIds.length; i++) {
                if (clientIds[i] === deviceToken) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                clientIds.push(deviceToken);
                userDetail.clientIds = clientIds;
                userDetail.save(function(err) {
                    if (err) {
                        var error = {};
                        error.code = 112;
                        error.message = "Error saving in DB";
                        return res.json(error);
                    } else {
                        res.json(true);
                    }
                });
            }
            logger.info("Client has been registered");
            res.json(true);
        } else {
            logger.error("User does not exist");
        }
        res.json(false);
    });

};


userRoute.forgotPasswordRequest = function(req, res) {
    if (!req.body.hasOwnProperty("email")) {
      logger.error("[Forgot Password] Invalid request parameters" + email);
      res.status(400);
      return res.send({
          "message": "Invalid request parameters"
      });
    }
    var email = req.body.email;
    logger.debug("[Forgot Password] Password reset request started for " + email);
    if (emailUtil.validateEmail(email)) {
        User.findOne({
            email: email
        }, function(err, user) {
          if (err) {
            logger.error("[Forgot Password] Error retrieving user with email *" + email + "*");
            return serverError(res);
          } else if (user === null) {
              logger.error("[Forgot Password] *" + email + "* not found");
              res.status(400);
              return res.send({
                  "message": email + " is not a registered email address"
              });
            } else if (user) {

              var verifierToken = securityUtil.generateVerifierToken();
              user.verifierToken = verifierToken;
              user.verifierTokenTimestamp = Date.now();
              user.save(function(err, obj) {
                  if (err) {
                      logger.error("[Forgot Password] Saving user Failed:" + JSON.stringify(err));
                      return serverError(res);
                  } else {
                      logger.info(" [Forgot Password] " + user.email + " updated with token for password reset");
                      var jwTokenForForgotPwd = securityUtil.generateJWTokenForForgotPwd(user.email, verifierToken);
                      var payload = {
                          email: user.email,
                          hostUrl: commonUtil.getHostUrl(),
                          token: jwTokenForForgotPwd
                      };

                      emailUtil.sendForgotPasswordEmail(payload).then(function() {
                          logger.info(" [Forgot Password] Forgot Password email sent to " + payload.email);
                          res.status(200);
                          return res.send({
                              "message": "Please check your email " + user.email + " to reset your password password"
                          });
                      }).catch(function(error) {
                          logger.error("[Forgot Password] Failed to send email to " + payload.email + " :" + JSON.stringify(error));
                          return serverError(res);
                      });
                  }
              });
            }
        });
    }
};

userRoute.forgotPasswordVerify = function(req, res) {
    logger.debug("[Forgot Password Verification] Password reset Verification started");
    var message = {};
    var decodedPayload = null;
    try{
      decodedPayload = securityUtil.decodeForgotPwdToken(req.query.t);
      logger.info(" [Forgot Password Verification] Request received for email " + JSON.stringify(decodedPayload));
    }catch(err){
      if(err.expiredAt){
        logger.error("[Forgot Password Verification] Token Expired at " + err.expiredAt);
      }else {
        logger.error("[Forgot Password Verification] Token decoding error " + JSON.stringify(err));
      }
      message.title = "Sorry, password reset failed :(";
      message.body = "Password reset token is not valid or expired, please try to reset password again";
      return res.render("password-reset-failure.html", {
          message: message,
          success: false
      });
    }
    if(decodedPayload){
      User.findOne({
            email: decodedPayload.email
        }, function(err, user) {
            if (err) {
                logger.error("[Forgot Password Verification] Token retrieval failed from user collection:" + JSON.stringify(err));
                message.title = "Sorry, password reset failed!";
                message.body = "There is an issue while processing your request. Please try again.";
                res.render("password-reset-failure.html", {
                    message: message,
                    success: false
                });
            } else {
                if (user !== null) {
                  if(user.verifierToken === decodedPayload.verifierToken){
                    logger.info(" [Forgot Password Verification] Token verified successfuly");
                    res.render("password-reset.html", {
                        message: "success",
                        success: true,
                        token: req.query.t
                    });
                  }else{
                    logger.error("[Forgot Password Verification] verifierToken doesn't match, verifierToken found : " + user.verifierToken);
                    message.title = "Sorry, password reset failed!";
                    message.body = "Password reset token is not valid or expired, please try to reset password again";
                    res.render("password-reset-failure.html", {
                        message: message,
                        success: false
                    });
                  }
                } else{
                  logger.error("[Forgot Password Verification] email not found in user collection");
                  message.title = "Sorry, Password Reset Failed!";
                  message.body = "Password reset token is not valid or expired, please try to reset password again";
                  res.render("password-reset-failure.html", {
                      message: message,
                      success: false
                  });
                }
          }
    });
} else{
  logger.error("[Forgot Password Verification] Payload decoding failed from token");
  message.title = "Sorry, password reset failed!";
  message.body = "Password reset token is not valid or expired, please try to reset password again";
  res.render("password-reset-failure.html", {
      message: message,
      success: false
  });
}
};

userRoute.forgotPasswordPerformReset = function(req, res) {
  if (!req.body.hasOwnProperty("pwd")) {
    logger.error("[Forgot Password Reset] Invalid request parameter -> pwd");
    res.status(400);
    return res.send({
        "message": "Invalid request parameters"
    });
  }
  if (!req.body.hasOwnProperty("token")) {
    logger.error("[Forgot Password Reset] Invalid request parameter -> t");
    res.status(400);
    return res.send({
        "message": "Invalid request parameters"
    });
  }

    logger.info(" [Forgot Password Reset] Password reset request process started");
    var message = {};
    var decodedPayload = null;
    try{
      decodedPayload = securityUtil.decodeForgotPwdToken(req.body.token);
      logger.info(" [Forgot Password Reset] Request received for email " + JSON.stringify(decodedPayload));
    }catch(err){
      logger.error("[Forgot Password Reset] Token Expired" + JSON.stringify(err));
      message.title = "Sorry, Password Reset Failed :(";
      message.body = "Password reset token is not valid or it is expired, please try to reset password again";
      return res.render("password-reset-failure.html", {
          message: message,
          success: false
      });
    }
    if(decodedPayload){
      User.findOne({
            email: decodedPayload.email
        }, function(err, user) {
            if (err) {
                logger.error("[Forgot Password Reset] Token retrieval failed from user collection:" + JSON.stringify(err));
                message.title = "Sorry, Password Reset Failed!";
                message.body = "There is an issue while processing your request. Please try again.";
                res.render("password-reset-failure.html", {
                    message: message,
                    success: false
                });
            } else {
                if (user !== null) {
                  if(user.verifierToken === decodedPayload.verifierToken){
                    logger.info(" [Forgot Password Reset] Token verified successfuly");
                    securityUtil.generateHash(req.body.pwd, function(err, hash) {
                        if (err) {
                            logger.error("[Forgot Password Reset] Password Hashing Error: " + JSON.stringify(err));
                            return serverError(res);
                        } else {
                            user.pwd = hash;
                            user.verifierToken = null;
                            user.verifierTokenTimestamp = Date.now();
                            try{
                              securityUtil.generateAuthToken(user.name, user.email);
                            }catch(error){
                              logger.error("[Forgot Password Reset] Error generating auth token" + JSON.stringify(error));
                              return serverError(res);
                            }
                            user.save(function(err, obj) {
                                if (err) {
                                    logger.error("[Forgot Password Reset] Saving user during password reset failed:" + JSON.stringify(err));
                                    return serverError(res);
                                } else {
                                    logger.info(" [Forgot Password Reset] password reset complete successfully for " + obj.email );
                                    res.send({ message: "You have successfully reset your password!"});
                                    var user = {
                                        email: obj.email,
                                        hostUrl: commonUtil.getHostUrl()
                                    };
                                    emailUtil.sendPasswordResetDoneEmail(user).then(function() {
                                        logger.info(" [Forgot Password Reset] Password reset confirmation email sent to " + obj.email);
                                    }).catch(function(error) {
                                        logger.error("[Forgot Password Reset] Failed to send password reset confirmation email to " + obj.email + ":Detaills" + JSON.stringify(error));
                                        //return serverError(res);
                                    });
                                }
                            });
                        }
                    });
                  }else{
                    message.title = "Sorry, Password Reset Failed!";
                    message.body = "Password reset token is not valid or expired, please try to reset password again";
                    res.render("password-reset-failure.html", {
                        message: message,
                        success: false
                    });
                  }
                } else{
                  logger.error("[Forgot Password Reset] email not found in user collection");
                  message.title = "Sorry, Password Reset Failed!";
                  message.body = "Password reset token is not valid or expired, please try to reset password again";
                  res.render("password-reset-failure.html", {
                      message: message,
                      success: false
                  });
                }
          }
    });
} else{
  logger.error("[Forgot Password Reset] Payload decoding failed from token");
  message.title = "Sorry, Password Reset Failed!";
  message.body = "Password reset token is not valid or expired, please try to reset password again";
  res.render("password-reset-failure.html", {
      message: message,
      success: false
  });
}
};

module.exports = userRoute;
