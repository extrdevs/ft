// Route -/place
var rek = require("rekuire");
var mongoose = require("mongoose");
var geolib = require("geolib");
var Place = mongoose.model("Place");
//var indexing = rek("redis-indexing");

var CONFIG = rek("data-config");
var CITIES = rek("cities");
var logger = rek("logger");

//TODO document all API, there access info and other details
var placeRoute = {};

/**
 * Add new place.
 * @param   latitude
 * @param   longitude
 */
placeRoute.addPlace = function(req, res) {
    //TODO add more validation
    logger.debug(req.body.name);
    if (!req.body.hasOwnProperty("name") ||
        !req.body.hasOwnProperty("lng") || !req.body.hasOwnProperty("lat")) {
        res.statusCode = 400;
        return res.send("Error 400: Post syntax incorrect");
    }

    var phone = [];
    if (req.body.hasOwnProperty("phone1")) phone.push(req.body.phone1);
    if (req.body.hasOwnProperty("phone2")) phone.push(req.body.phone2);
    if (req.body.hasOwnProperty("phone3")) phone.push(req.body.phone3);

    var area1 = undefined;
    var area2 = undefined;
    var area3 = undefined;
    var area4 = undefined;
    var area5 = undefined;
    var area6 = undefined;

    if (req.body.hasOwnProperty("area1")) {
        area1 = req.body.area1;
    }
    if (req.body.hasOwnProperty("area2")) {
        area2 = req.body.area2;
    }
    if (req.body.hasOwnProperty("area3")) {
        area3 = req.body.area3;
    }
    if (req.body.hasOwnProperty("area4")) {
        area4 = req.body.area4;
    }
    if (req.body.hasOwnProperty("area5")) {
        area5 = req.body.area5;
    }
    if (req.body.hasOwnProperty("area6")) {
        area6 = req.body.area6;
    }

    place = new Place({
        name: req.body.name,
        type: req.body.type,
        lng: req.body.lng,
        lat: req.body.lat,
        country: req.body.country,
        state: req.body.state,
        city: req.body.city,
        street: req.body.street,
        area1: area1,
        area2: area2,
        area3: area3,
        area4: area4,
        area5: area5,
        area6: area6,
        zip: req.body.zip,
        status: req.body.status,
        phone: phone,
        image: {
            imgId: req.body.imgId,
            imgVer: ""
        },
        author: req.user._id,
        likes: 0,
        likedBy: []
    });

    place.save(function(err, p) {
        if (err) {
            logger.debug("Error while saving post Document");
            res.json(false);
        } else {
            res.json(true);
        }
    });
};


//utility function to get place list(mainly for testing)
placeRoute.getAllPlaces =  function(req, res) {

    var x = "sdf";
    Place
        .find({
            "name": x
        })
        .exec(function(err, places) {
            if (err) {
                return console.error(err);
            } else if (places.length > 0) {

                var placesDao = [];
                for (var i = 0; i < places.length; i++) {

                    placesDao.push({
                        "id": places[i]._id,
                        "name": places[i].name,
                        "type": places[i].type,
                        "city": places[i].city,
                        "state": places[i].state

                    });
                }
                res.json(placesDao);
            } else {
                res.json({});
            }
        });
};

/**
 * Get the list of places containing the string passed.
 * Its place sugession functionality
 * @param term: string to be searched.
 * @param type: Type of the place.
 */
/*placeRoute.getSuggestedPlaces = function(req, res) {

    var term = req.query.term;
    var type = req.query.type;

    logger.debug("term:" + term);
    logger.debug("type:" + type);

    if (!term){
        res.statusCode = 400;
        return res.send("Error 400: Place Term is missing");
    }

    indexing.completer.search({
        search: term,
        ns: "area"
    }, function(err, results) {

        if (type !== undefined && type !== "") {

            for (var i = results.length - 1; i >= 0; i--) {
                if (results[i].type != type) {
                    results.splice(i, 1);
                }
            }
        }
        res.json(results);
    });

};*/


//utility function to get places(restaurents) sorted by distance
placeRoute.getNearbyPlaces = function(req, res) {
    var lng = req.query.lng;
    var lat = req.query.lat;

    var blankString = new RegExp(".*");
    var city = blankString;
    var type = -1;
    var radius = 111111000000;
    if (req.query.city) city = req.query.city;
    if (req.query.type) type = req.query.type;
    if (req.query.radius) radius = req.query.radius;
    logger.debug(city);
    logger.debug(type);
    logger.debug(radius);
    logger.debug(lat);
    logger.debug(lng);

    var query = (type == -1 ? {
        "city": city
    } : {
        "city": city,
        "type": type
    });

    Place
        .find(query)
        .exec(function(err, places) {
            //logger.debug(places)
            if (err) {
                return console.error(err);
            } else if (places.length > 0) {

                var placesDao = [];
                for (var i = 0; i < places.length; i++) {
                    //logger.debug(places[i].lattitude)
                    //logger.debug(places[i].longitude)
                    var distance = geolib.getDistance({
                        latitude: lat,
                        longitude: lng
                    }, {
                        latitude: places[i].lat,
                        longitude: places[i].lng
                    });
                    logger.debug("dist-->" + distance);
                    if (distance <= radius) {
                        placesDao.push({
                            "_id": places[i]._id,
                            "name": places[i].name,
                            //"type": places[i].type,
                            //"city": places[i].city,
                            //"state": places[i].state,
                            "distance": distance
                        });
                    }
                }

                placesDao.sort(function(a, b) {
                    return parseFloat(a.distance) - parseFloat(b.distance);
                });

                //adding order attribute in sorted places by distance
                for (var j = 0; j < placesDao.length; j++) {
                    placesDao[j].order = j + 1;
                    placesDao[j].display = placesDao[j].name;
                }

                res.json(placesDao);
            } else {
                res.json({});
            }
        });

};

placeRoute.getPlaceTypes = function(req, res) {

    /*for (var i = 0; i < CONFIG.PLACE_TYPE.length; i++) {
        CONFIG.PLACE_TYPE[i].order = i + 1;
    }*/

    res.json(CONFIG.PLACE_TYPE);
};


placeRoute.getCities = function(req, res) {

    //var placeTypesDto = [];

    for (var i = 0; i < CITIES.length; i++) {
        CITIES[i].order = i + 1;
        CITIES[i].display = CITIES[i].name;
    }

    res.json(CITIES);
};

placeRoute.getAreas = function(req, res) {

    var city = req.query.city;

    for (var i = 0; i < CITIES.length; i++) {
        CITIES[i].order = i + 1;
        CITIES[i].display = CITIES[i].name;
    }

    res.json(CITIES);
};

////////////////////////////////////////////
module.exports = placeRoute;
