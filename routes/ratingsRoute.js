var rek = require("rekuire");
var mongoose = require("mongoose");
var logger = rek("logger");

var Post = mongoose.model("Post");
var UserDetail = mongoose.model("UserDetail");

var ArrFun = require("../js/arrFun.js");

var ratingsRoute = {};
//give ratings to post
ratingsRoute.getPostRating = function(req, res, user) {
    var postId = req.body.postId;
    //logger.debug(liked);
    var userId = req.user._id;
    //logger.debug(liked);
    var rat = req.body.rat;
    //logger.debug(rat);
    Post.findById(postId, function(err, p) {
        if (!p) return next(new Error("Could not load Document of given Post"));
        else {
            UserDetail.findOne({
                "secondaryId": userId
                })
            .exec(function(err, useDetail){
                logger.debug("got user");
                var arr = useDetail.ratings;
                var rating = {
                    postId : mongoose.Types.ObjectId(postId),
                    rating : rat
                };
                arr.push(rating);
                useDetail.ratings = arr;
                useDetail.save(function(err) {
                    if (err)  logger.debug("Unable to save useDetail Document");
                    else {
                        logger.debug("saved user");
                        var t = p.ratings;
                        var rating = {
                            userId : mongoose.Types.ObjectId(userId),
                            rating : rat
                        };
                        t.push(rating);

                        p.ratings = t;
                        p.save(function(err) {
                            if (err)  logger.debug("Unable to save Post Document");
                            else{
                                logger.debug("success2");
                                res.json(p);
                            }
                        });
                    }
                });
            });
        }
    });
};

//update ratings to post(currently without authentecation)
ratingsRoute.updatePostRating = function(req, res, user) {
    var postId = req.body.postId;
    var userId = req.user._id;
    var rat = req.body.rat;

    logger.debug(userId);
    logger.debug(rat);

    Post.findById(postId, function(err, p) {
        if (!p) return next(new Error("Could not load Document of given Post"));
        else {
            UserDetail.findOne({
                "secondaryId": userId
                })
            .exec(function(err, useDetail){
                logger.debug("got user");

                var arr = useDetail.ratings;
                var arr = ArrFun.updateArray(arr, postId, rat);
                //logger.debug("posrArray")
                //logger.debug(arr)
                useDetail.ratings = arr;
                useDetail.save(function(err) {
                    if (err)  logger.debug("Unable to save useDetail Document");
                    else {
                        logger.debug("saved user");
                        var t = p.ratings;
                        var t = ArrFun.updateArray2(t, userId, rat);
                        p.ratings = t;
                        p.save(function(err) {
                            if (err)  logger.debug("Unable to save Post Document");
                            else{
                                logger.debug("success2");
                                res.json(p);
                            }
                        });
                    }
                });
            });

        }

    });

};

// avg rating(currently without authentecation)
ratingsRoute.getAveragePostRating = function(req, res) {
    var postId = req.query.id;

    Post.findById(postId, function(err, p) {
        if (!p) return next(new Error("Could not load Document of given Post"));
        else {
            var t = p.ratings;
            var t = ArrFun.takeAvg(t);
            res.json(t);
        }

    });

};


module.exports = ratingsRoute;
