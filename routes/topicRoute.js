// Route -/topic
var string = require("string");
var rek = require("rekuire");
var logger = rek("logger");
var mongoose = require("mongoose");
var Topic = mongoose.model("Topic");

var topicRoute = {};
//Create Topic
topicRoute.createTopic = function(req, res) {

    if (!req.body.hasOwnProperty("name") ||
        !req.body.hasOwnProperty("info")) {
        res.statusCode = 400;
        return res.send("Error 400: Post syntax incorrect.");
    }

    var name = string(req.body.name).replaceAll(" ", "-");

    var topic = new Topic({
        name: name,
        info: req.body.info,
        author: req.user._id,
        followers: 1
    });

    topic.save(function(err) {
        if (err) {
            logger.debug("Topic creation Error!");
            res.json(false);
        } else {
            logger.debug("Topic Created");
            res.json(true);
        }
    });
};

module.exports = topicRoute;
