var should = require('should');


/**
 * Plugin to add Authentication header
 * @param  request obj
 */
function bearer(request) {
    if (token) {
        request.set('Authorization', 'Bearer ' + token);
    }
}

//Set request and token
before(function() {
    request = require("./../testConfig").request;
    token = require("./../testConfig").token;
});

it('should get places suggestion', function(done) {
    request
        .get('/place/suggest')
        .set('Accept', 'application/json')
        .query({
            term: 'pu',
            type: 3
        })
        .end(function(err, res) {
            should.not.exist(err);
            res.status.should.be.equal(200);
            if (res.status == 200) {

            }
            done();
        });
});

//Following is code to send request to specific url. Can be used when you want to 
/*    it('should get suggestion', function(done) {
        request('http://localhost:3000')
            .get('/place/suggest')
            .set('Accept', 'application/json')
            .query({
                term: 'in',
                type: 1
            })
            .end(function(err, res) {
                should.not.exist(err);
                res.status.should.be.equal(200);
               done();
            });
    });*/
