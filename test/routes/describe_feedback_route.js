var should = require('should');


/**
 * Plugin to add Authentication header
 * @param  request obj
 */
function bearer(request) {
    if (token) {
        request.set('Authorization', 'Bearer ' + token);
    }
}

//Set request and token
before(function() {
    request = require("./../testConfig").request;
    token = require("./../testConfig").token;
});

it('should insert feedback for Authenticated User', function(done) {
    request
        .post('/feedback')
        .use(bearer)
        .set('Accept', 'application/json')
        .send({
            comments: 'Test Comments',
            rating: 3,
            author: 'test'
        })
        .end(function(err, res) {
            should.not.exist(err);
            res.status.should.be.equal(200);
            if (res.status == 200) {
            }
            done();
        });
});

it('should insert feedback for Non-Authenticated User', function(done) {
    request
        .post('/feedback')
        .set('Accept', 'application/json')
        .send({
            comments: 'Test Comments',
            rating: 3,
            author: 'test',
            email: 'test@gmail.com'
        })
        .end(function(err, res) {
            should.not.exist(err);
            res.status.should.be.equal(200);
            if (res.status == 200) {

            }
            done();
        });
});

it('should give error as comments and rating both are missing', function(done) {
    request
        .post('/feedback')
        .set('Accept', 'application/json')
        .send({
            author: 'test',
            email: 'test@gmail.com'
        })
        .end(function(err, res) {
            res.status.should.be.equal(400);
            done();
        });
});
