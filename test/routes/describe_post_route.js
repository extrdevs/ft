var should = require('should');

/**
 * Plugin to add Authentication header
 * @param  request obj
 */
function bearer(request) {
    if (token) {
        request.set('Authorization', 'Bearer ' + token);
    }
}

//Set request and token
before(function() {
    request = require("./../testConfig").request;
    token = require("./../testConfig").token;
});

it('should get comment with given id', function(done) {
    request
        .get('/post/55bf146fbf1063d0245b63a7/com')
        .use(bearer)
        .set('Accept', 'application/json')
        .query({})
        .end(function(err, res) {
            //console.log("res", res);
            should.not.exist(err);
            res.status.should.be.equal(200);
            if (res.status == 200) {

            }
            done();
        });
});

it('should add comments to the post', function(done) {
    request
        .post('/post/postId/com')
        .use(bearer)
        .set('Accept', 'application/json')
        .send({
            postId : '55a7e71e10a40ec65a82b901',
            payload: 'Try to use another road for comute'
        })
        .end(function (err, res) {
            should.not.exist(err);
            res.status.should.be.equal(200);
            if (res.status == 200) {

            }
            done();
        });
});

it('should like/unlike the post', function(done) {
    request
        .put('/post/like')
        .use(bearer)
        .set('Accept', 'application/json')
        .send({
            itemId : '55a7e71e10a40ec65a82b901',
            like: 'false'
        })
        .end(function (err, res) {
            should.not.exist(err);
            res.status.should.be.equal(200);
            /*if (res.status == 200) {
                console.log(res.body);
            }*/
            done();
        });
});

it('should get post with given id', function(done) {
    request
        .get('/post/55a7e73110a40ec65a82b902')
        .use(bearer)
        .set('Accept', 'application/json')
        .query({})
        .end(function(err, res) {
            should.not.exist(err);
            res.status.should.be.equal(200);
            if (res.status == 200) {
                //console.log("res:", res.body);
            }
            done();
        });
});

it('should GET post for a given area', function(done) {
    request
        .get('/post/')
        .use(bearer)
        .set('Accept', 'application/json')
        .query({
            area : 'ChIJy9Nd8M--wjsRfat_-5cSkaE',
            max: 'now',
            count: 2
        })
        .end(function (err, res) {
            should.not.exist(err);
            res.status.should.be.equal(200);
            if (res.status == 200) {
               //console.log(res.body);
            }
            done();
        });
});


it('should GET post of a given user', function(done) {
    request
        .get('/post/')
        .use(bearer)
        .set('Accept', 'application/json')
        .query({
            userId : '558b9c06ead3bc0948ce00cd',
            max: 'now',
            count: 2
        })
        .end(function (err, res) {
            should.not.exist(err);
            res.status.should.be.equal(200);
            if (res.status == 200) {
               //console.log(res.body);
            }
            done();
        });
});

it('Should add posts for Baner', function(done) {
    var posts = getBanerPosts(3);
    for(var i = 0; i< posts.length; i++){
        request
            .post('/post/')
            .use(bearer)
            .set('Accept', 'application/json')
            .send(posts[i])
            .end(function (err, res) {
                should.not.exist(err);
                res.status.should.be.equal(200);
            });
    }
    done();
});

/* Adding post to our production service */
/*var superagent = require('superagent');
it.only('Should add 10 post for Baner', function(done) {
    var posts = getBanerPosts(10);
    for(var i = 0; i< posts.length; i++){
        superagent
            .post('http://api.placetalkr.com/post/')
            .use(bearer)
            .set('Accept', 'application/json')
            .send(posts[i])
            .end(function (err, res) {
                should.not.exist(err);
                res.status.should.be.equal(200);
                console.log("res: ", res.body);
            });
    }
});*/

/* Get posts for Baner area */
function getBanerPosts(count){
    var areaIdString = "ChIJy9Nd8M--wjsRfat_-5cSkaE";//Baner
    var typeString = "1";
    var areaName = 'Baner';
    var posts = getAreaPostsForTesting(count, areaIdString, areaName, typeString);
    ///console.log(posts);
    return posts;
}

/* Get dummy Posts for testing purpose */
function getAreaPostsForTesting(count, areaIdString, areaName, typeString){
    var posts = [];

    var title = 'Test Post ';
    var description = 'Test Post Description ';

    for(var i = 1; i<= count; i++){
        var post = getPost((title+i), (description+i), areaIdString, areaName, typeString);
        posts.push(post);
    }
    return posts;
}

/* Get Post object */
function getPost(title, description, areaIdString, areaName, typeString){
        var post = {
            title: title,
            des: description,
            type: typeString,
            area: areaIdString,
            areaName: areaName,
            imgId: undefined
     };
    return post;
}

/*
    Sample POst Object
    { _id: '55b150f83bcb4afd5176e36a',
    title: 'asdasda',
    des: 'a b c d',
    type: 1,
    area: '55a2c10f8335e55e3cc9f6f8',
    author: '558b9c06ead3bc0948ce00cd',
    likes: 1,
    commentscount: 1,
    __v: 9,
    modified: '2015-07-23T20:42:26.000Z',
    created: '2015-07-23T20:39:20.000Z',
    comLastId: 0,
    ratings: [],
    likedBy: [ '558b9c06ead3bc0948ce00cd' ],
    comCount: 0,
    comments: [] } */

