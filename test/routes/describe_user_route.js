var should = require('should');

/**
 * Plugin to add Authentication header
 * @param  request obj
 */
function bearer(request) {
    if (token) {
        request.set('Authorization', 'Bearer ' + token);
    }
}

//Set request and token
before(function () {
    request = require("./../testConfig").request;
    token = require("./../testConfig").token;
});

it('should get all user details', function (done) {
    request
        .post('/user/userDetails')
        .set('Accept', 'application/json')
        .send()
        .end(function (err, res) {
            should.not.exist(err);
            res.status.should.be.equal(200);
            if (res.status == 200) {

            }
            done();
        });
});

/*        
var url = 'http://localhost:3000/';
it('Should create entry for new user', function(done) {
            request(url)
                .post('user')
                .set('Accept', 'application/json')
                .send({
                    name: 'Arpit',
                    email: 'arpitgawande@gmail.com',
                    pwd: 'test123'
                })
                .expect(200)
                .end(function(err, res) {
                    if (err) throw err;
                    done();
                });
        });

        it('Should validate new user', function(){
                request(url)
                .get('user/verify/12345')
                .set('Accept', 'application/json')
                .expect(200)
                .end(function(err, res) {
                    if (err) throw err;
                });
        });*/

it('Should reset password of the user', function (done) {
    request
        .post('/auth/forgotPassword')
        .set('Accept', 'application/json')
        .send({
            email: 'extrdevs@gmail.com',
        })
        .expect(200)
        .end(function (err, res) {
            if (err) throw err;
            done();
        });
});

it('Should Update profile of the user', function (done) {
    request
        .put('/user/')
        .use(bearer)
        .set('Accept', 'application/json')
        .send({
            name: 'Extr Updated',
            email: 'extrdevs@gmail.com',
            pwd: 'extr'
        })
        .expect(200)
        .end(function (err, res) {
            if (err) throw err;
            done();
        });
});

it('Should Update register client to user', function (done) {
    request
        .post('/user/registerClient')
        .use(bearer)
        .set('Accept', 'application/json')
        .send({
            deviceToken: 'APA91bEtqNDuTaXdML2L_yi2ymSGYrVseTM0ilcxZlt4rDkcYUwTrSmZ8XLuDf6F_eAlzEDDd-P5zl7Sl591cnQKTFSLx7rHkWxwHLw2VVJmcaKNeRnF9j8'
        })
        .expect(200)
        .end(function (err, res) {
            if (err) throw err;
            done();
        });
});
