var should = require('should');
var testServer = require('./testServer');
var util = require('util');
var async = require('async');
var chalk = require('chalk');

var rek = require('rekuire');
var logger = rek('logger');

var testConfig = require("./testConfig");

var token = "";
var request = {};

//var feedback = require('./describe_feedback_route');

var agent = {};

var testuser = {};
testuser.login = 'extrdevs@gmail.com';
testuser.password = 'extr';

/**
 * Plugin to add Authentication header
 * @param  request obj
 */
function bearer(request) {
    if (token) {
        request.set('Authorization', 'Bearer ' + token);
    }
}

describe('Test Preparation', function() {

    /**
     * Starting tests server and setting up required poperties.
     */
    before(function(done) {
		this.timeout(20000);
        async.series({

            startTestServer: function(callback) {
                console.info(chalk.yellow('Starting test server >>>>>>'));

                testServer.start(callback);
                request = testConfig.request = testServer.request;
            },

            setAuthenticationToken: function(callback) {
                console.info(chalk.yellow('Getting Authentication Toekn >>>>>>'));

                request
                    .post('/auth/login')
                    .set('Accept', 'application/json')
                    .send({
                        username: testuser.login,
                        password: testuser.password
                    })
                    .end(function(err, res) {
                        should.not.exist(err);
                        res.status.should.be.equal(200);
                        should.not.exist(res.headers['set-cookie']);
                        //logger.debug(res.body.token);
                        //Setting Authentication token.
                        token = testConfig.token = res.body.token
                        callback(null, 2);
                    });
            }

        }, function(err, results) {
            if (err) logger.debug('err = ' + err);
            logger.debug('results = ', results);
            logger.debug(chalk.yellow('Starting Execution >>>>>>'));
            done();
        });
    });

    /**
     *  Complete Test Suite
     */
    describe('Test Suite', function() {
        this.timeout(50000);
        
        describe('Feeddback', function() {
            require('./routes/describe_feedback_route');
        });

        describe('Place', function() {
            require('./routes/describe_place_route');
        });

        describe("User", function() {
            require('./routes/describe_user_route');
        });

        describe("Post", function() {
            require('./routes/describe_post_route');
        });

    });

    /*
     * Stop server after all tests are perfrmed
     */
    after(function(done) {
        testServer.stop(done);
    });

});
