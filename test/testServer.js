var request = require('supertest');
var rek = require('rekuire');
var em = rek("event-manager");
var rek = require('rekuire');
var logger = rek('logger');

//Get app for testing
var app = require('../app').app;

var port = 3333;

var server = {};

module.exports = {

    request: request(app),

    start: function(callback) {
        logger.debug('Starting Test Server');
        server = app.listen(port, function(err, result) {
            if (err) {
                logger.error("Error starting server at: " + port);
                done(err);
                callback(null, 1);
            } else {
                em.ee.on('datastore-all-done', function() {
                    logger.debug("Server started listening at: " + port);
                    callback(null, 1);
                });
            }
        });
    },

    stop: function(done) {
        logger.debug('Closing the server...');
        server.close();
        done();
    }

};
