var Code = require('code');
var Lab = require('lab');

var lab = exports.lab = Lab.script();

var describe = lab.describe;
var it = lab.it;
var before = lab.before;
var after = lab.after;
var expect = Code.expect;
var rek = require('rekuire');

//Register models which are going to get used in any of the called functions.
rek('userDetailModel');
var util = rek('push-notification-util');
var mongoose = require('mongoose');
var connectionURL = 'mongodb://127.0.0.1:27017/test';

describe('PushNotificationTest', function () {

    before(function (done) {
        mongoose.connect(connectionURL);
        var conn = mongoose.connection;
        conn.on('error', console.error);
        conn.once('open', function () {
            console.log('Mongo Connection created at: ', connectionURL);
            done();
        });
    });


    it('Should send notification to client device using google messaging when adding comment', function (done) {
        var post = {
            _id: '55a7e73110a40ec65a82b902',
            author: '559ecbb7a8f134475a15d5d0'
        }
        var message = {
                title: 'Test message notification',
                body: 'Your question has been answered',
                postId: '55a7e73110a40ec65a82b902'
            }
        util.commentNotification(post, message);
    });

    after(function(done){
        console.log('Closing Mongo connection');
        mongoose.connection.close();
        done();
    });


});
