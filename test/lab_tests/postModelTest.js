var Code = require('code');
var Lab = require('lab');

var lab = exports.lab = Lab.script();

var describe = lab.describe;
var it = lab.it;
var before = lab.before;
var after = lab.after;
var expect = Code.expect;

var Post = require('../../models/postModel');
var mongoose = require('mongoose');
var connectionURL = 'mongodb://127.0.0.1:27017/test';

describe('Save', function () {

    before(function (done) {
        mongoose.connect(connectionURL);
        var conn = mongoose.connection;
        conn.on('error', console.error);
        conn.once('open', function () {
            console.log('Mongo Connection created at: ', connectionURL);
            done();
        });
    });

    it('Should create a new the post', function (done) {
        Post.create({
            title: 'Post Test',
            des: 'Test details',
            type: 1,
            area: '55da36f519250dc71fdc113f'
        });
        done();
    });

    after(function(done){
        console.log('Closing Mongo connection');
        mongoose.connection.close();
        done();
    });

});
