"use strict";
var express = require("express");
var path = require("path");
var favicon = require("serve-favicon");
var bodyParser = require("body-parser");
var rek = require("rekuire");
//var cache = require("cache-control");
var ejs = require("ejs");
var em = rek("event-manager");
//
var helmet = require("helmet");

require("./config/init")();
var CONFIG = require("./config/config");
var logger = require("./config/logger");
var passport = require("passport");

var app = exports.app = express();

//Initialize Datastores
require("./config/datastores").init();

em.ee.on("datastore-all-done", function() {

    logger.info(" [Bootstrap] Datastore init done");

    //*****View Engine Setup****
    app.set("view engine", "ejs");
    app.engine(".html", ejs.renderFile);
    /*app.set("views", path.join(__dirname, "views"));
    app.set("view engine", "jade");*/

    //  Handling CORS (Cross Origin Resource Sharing)
    //  callback to set up CORS related headers

    var allowCrossDomain = function(req, res, next) {
        res.header("Access-Control-Allow-Origin", CONFIG.CORS.ALLOWED_DOMAINS || "*");
        res.header("Access-Control-Allow-Methods", CONFIG.CORS.ALLOWED_METHODS ||
                        "GET, PUT, POST, DELETE, OPTIONS");
        res.header("Access-Control-Allow-Headers", CONFIG.CORS.ALLOWED_HEADERS || "*");
        //deal with OPTIONS method
        if (req.method == "OPTIONS") {
          res.send(200);
        } else {
          next();
        }
    };

    app.use(allowCrossDomain);

    //*** handle OPTIONS requests from the browser ***
    app.options("*", function(req, res, next) {
        res.send(200);
    });

    //set security-related HTTP headers
    app.use(helmet());

    app.use(require("morgan")("dev",
        {"skip": function (req, res) {
            return res.statusCode < 400;
        }
    }));

    // set cache-controll headers
    /*app.use(cache({
         "/**": false,
    }));*/

    //*** Other Config Setup ***
    app.use(favicon(__dirname + "/public/images/favicon.ico"));
    //app.use(logger("dev"));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: true
    }));

    app.use(express.static(path.join(__dirname, "public")));

    //*** Auth Setup ***
    logger.info(" [Bootstrap] Setting passport");
    //passport config
    require("./config/passport")(passport, app);

    logger.info(" [Bootstrap] Setting routes");
    require("./config/routes")(app);

    //*** End Auth Setup ***

    /// catch 404 and forward to error handler
    app.use(function(req, res, next) {
        var err = new Error("Not Found");
        err.status = 404;
        next(err);
    });

    //******* Error Handlers *********
    // Development error handler
    // if (app.get("env") === "development") {
    //     app.use(function(err, req, res, next) {
    //         logger.error(err);
    //         res.status(err.status || 500);
    //         res.send({message: err.message || "Error",
    //                 code: err.errorCode || err.status});
    //     });
    // }

    // Production error handler
    app.use(function(err, req, res, next) {
        logger.error("Error caught at app level: " + err);
        res.status(err.status || 500);
        res.send({message: err.message || "Error",
                code: err.errorCode || err.status});
    });
    //******* END Error Handlers **********

    if (!module.parent) {

        //var server = {};

        if (CONFIG.APP_SERVER.HOST) {
            app.listen(CONFIG.APP_SERVER.PORT, CONFIG.APP_SERVER.HOST, function() {
                logger.info(" [Bootstrap] Server is listening on port:" + CONFIG.APP_SERVER.PORT + "& host:" + CONFIG.APP_SERVER.HOST);
                logger.info(" [Bootstrap] ***All Ready to Serve***");
            });
        } else {
            app.listen(CONFIG.APP_SERVER.PORT, function() {
                logger.info(" [Bootstrap] Server is listening on port:" + CONFIG.APP_SERVER.PORT);
                logger.info(" [Bootstrap] *** All Ready to Serve ***");
            });
        }
    } else{
      /*
       module.parent check added for using same app.js for testing APIs.
       This value not null if test module is invoking it. So this provision is to not to start server from
       this file when running test because mocha starts it from its enviornment.
      */
      logger.debug("[Test] module.parent: " + module.parent);

    }

});
