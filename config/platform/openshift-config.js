"use strict";
var rek = require("rekuire");
var logger = rek("logger");


/**
 * Setting configuration parameter for external dependancy such as DB also some web app.
 * parameters. 
 * @return Enviornment variables
 *
 * OpenShift platform provides several environment variables which are populated
 * and ready to be used in the code. List is at below link
 * https://developers.openshift.com/en/managing-environment-variables.html
 *
 *  There are three ways to view the environment variables for an application:
 *   1. Add an export statement to the App_Name/.openshift/action_hooks/build file,
 *   then run git push.
 *   The variables are listed in the Git output and start with remote: declare -x.
 *   2. Access the application with SSH and run the env command at the shell prompt.
 *   3. Use the rhc env list -a <appname> command

 * If the provided environment variables do not satisfy your need,
 * you can proceed to creating custom environment variables by using the rhc set-env command.
    rhc set-env VARIABLE1=VALUE1 VARIABLE2=VALUE2 -a myapp --namespace domain
 */
module.exports = function() {
    logger.debug("Loading openshift config.....");
    //	process.env.OPENSHIFT_NODEJS_PORT = 3000;
    //	process.env.OPENSHIFT_NODEJS_IP =
    //	process.env.OPENSHIFT_NODEJS_POLL_INTERVAL =
    process.env.NPM_CONFIG_PRODUCTION = "true";
    //MONGO DB Config
    process.env.MONGO_USERS_NAME = process.env.OPENSHIFT_MONGODB_DB_USERNAME;
    process.env.MONGO_PASSWORD = process.env.OPENSHIFT_MONGODB_DB_PASSWORD;
    process.env.MONGO_HOST = process.env.OPENSHIFT_MONGODB_DB_HOST;
    process.env.MONGO_PORT = process.env.OPENSHIFT_MONGODB_DB_PORT;
    process.env.MONGO_DB = "nodejs";
    //REDIS Config
    process.env.REDIS_HOST = process.env.OPENSHIFT_REDIS_HOST;
    process.env.REDIS_PORT = process.env.OPENSHIFT_REDIS_PORT;
    process.env.REDIS_PASSWORD = process.env.REDIS_PASSWORD;
    //Host Config
    process.env.APP_PORT = process.env.OPENSHIFT_NODEJS_PORT;
    process.env.APP_HOST = process.env.OPENSHIFT_NODEJS_IP
    process.env.APP_HOST_NAME = process.env.OPENSHIFT_NODEJS_HOST_NAME;
};
