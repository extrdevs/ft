"use strict";
var url = require("url");
var rek = require("rekuire");
var logger = rek("logger");

/**
 * Setting configuration parameter for external dependancy such as DB also some web app
 * parameters.
 * @return Enviornment variables
 *
 * In Heroku many config parameter are need to be set/reset by the users,
 * which can be done using following commads or from the settings option on web dashborad.

 * config:get KEY                            #  display a config value for an app
 * config:set KEY1=VALUE1 [KEY2=VALUE2 ...]  #  set one or more config vars
 * config:unset KEY1 [KEY2 ...]              #  unset one or more config vars

 * To see all the current configuration settings use following command.
 * heroku config
 *
 * process.env.PORT contain the default port to which server is bind this parameter is set
 * by heroku itself

 */
module.exports = function() {

    logger.debug("Loading Heroku config.....");

    process.env.NPM_CONFIG_PRODUCTION = "true";

    //REDIS Config
    var redisURL = url.parse(process.env.REDIS_URL);
    process.env.REDIS_HOST = redisURL.hostname;
    process.env.REDIS_PORT = redisURL.port;
    process.env.REDIS_PASSWORD = redisURL.auth.split(":")[1];

    //MONGO DB Config
    var mongoURL = url.parse(process.env.MONGOLAB_URI)
    process.env.MONGO_USERS_NAME = mongoURL.auth.split(":")[0] || "webuser";
    process.env.MONGO_PASSWORD = mongoURL.auth.split(":")[1] || "webuser";
    process.env.MONGO_HOST = mongoURL.hostname || "ds049198.mongolab.com";
    process.env.MONGO_PORT = mongoURL.port || "49198";
    process.env.MONGO_DB = mongoURL.pathname.split("/")[1] || "heroku_pv62t3rm";

    //Host Config
    process.env.APP_PORT = process.env.PORT;
    process.env.APP_HOST = "";
    process.env.APP_HOST_NAME = "";
};
