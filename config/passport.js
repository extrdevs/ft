var mongoose = require("mongoose");
var User = mongoose.model("User");

var local = require("./passport/local");
var bearer = require("./passport/bearer");
var google = require("./passport/google");
var facebook = require("./passport/facebook");


module.exports = function (passport, app) {

//middleware to initialize Passport.
app.use(passport.initialize());

// Passport session setup.
//If your application uses persistent login sessions,
//passport.session() middleware must also be used.

//app.use(passport.session()); -->session in not used.

//In a typical web application, the credentials used to authenticate a user will only
//be transmitted during the login request. If authentication succeeds, a session will
//be established and maintained via a cookie set in the user"s browser.
//Each subsequent request will not contain credentials, but rather the unique cookie
//that identifies the session. In order to support login sessions, Passport will
//serialize and deserialize user instances to and from the session.

  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    User.find({ criteria: { _id: id } }, function (err, user) {
      done(err, user);
    });
  });

  // use these strategies
  passport.use(local);
  passport.use(bearer);
  passport.use(google);
  passport.use(facebook);
};
