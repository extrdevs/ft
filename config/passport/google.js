var mongoose = require("mongoose");
var User = mongoose.model("User");
var UserDetail = mongoose.model("UserDetail");
var GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;
var rek = require("rekuire");
var logger = rek("logger");
var InsufficientDataError = rek("InsufficientDataError");


// API Access link for creating client ID and secret:
// https://code.google.com/apis/console/
var GOOGLE_CLIENT_ID = "716972706999-0k28kdvhpacd4pmtire57tqhs55as61l.apps.googleusercontent.com";
var GOOGLE_CLIENT_SECRET = "rXBOC7QToi3M22Sq-92AyLJs";

// Use the GoogleStrategy within Passport.
//   Strategies in Passport require a `verify` function, which accept
//   credentials (in this  , GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;case, an accessToken, refreshToken, and Google
//   profile), and invoke a callback with a user object.
module.exports = new GoogleStrategy({
    clientID: GOOGLE_CLIENT_ID,
    clientSecret: GOOGLE_CLIENT_SECRET,
    callbackURL: "/auth/google/callback"
},
function (accessToken, refreshToken, profile, done) {
    // asynchronous verification, for effect...
    if (profile.emails === undefined || profile.emails === null) {
        var err = new InsufficientDataError("Email id must be provided", 400);
        return done(err, null);
    }
    logger.debug("Executing Google Strategy for", profile.emails[0].value);
    process.nextTick(function () {
        User.findOneAndUpdate({
          email: profile.emails[0].value
        }, {accessToken: accessToken}, function(err, usr) {
            if (err) return done(err);
            if (!usr) {
                logger.debug("Creating new user");
                var user = new User();
                user.name = profile.displayName;
                user.email = profile.emails[0].value;
                user.pwd = "";
                user.accessToken = accessToken;

                user.save(function(err, obj) {
                    if (err) {
                        logger.debug("Error saving new user confirmation details!", err);
                        return done(err, user);
                    } else {
                        //once primary user object is created create secondary user object
                        //check if this will affect the performance while sign up
                        var userDetail = new UserDetail();
                        userDetail.secondaryId = obj.id;
                        userDetail.name = profile.displayName;
                        userDetail.save(function(err) {
                            if (err) return done(err, user);
                            return done(null, user);
                        });
                    }
                });
            } else {
                logger.debug("Existing user: ", usr.email);
                return done(err, usr);
            }
        });

    });
});


//Google+ userinfo - as of July 2015
/*{
 "kind": "plus#person",
 "etag": "\"RqKWnRU4WW46-6W3rWhLR9iFZQM/nt6PiK78emefd-B6p6KaU8GKvlQ\"",
 "gender": "male",
 "emails": [
  {
   "value": "arpitgawande@gmail.com",
   "type": "account"
  }
 ],
 "urls": [
  {
   "value": "http://www.google.com/reader/shared/06473287554770400489",
   "type": "otherProfile",
   "label": "Google Reader"
  }
 ],
 "objectType": "person",
 "id": "106458255161584063578",
 "displayName": "Arpit Gawande",
 "name": {
  "familyName": "Gawande",
  "givenName": "Arpit"
 },
 "url": "https://plus.google.com/+ArpitGawande",
 "image": {
  "url": "https://lh5.googleusercontent.com/-EApK9ZssVNc/AAAAAAAAAAI/AAAAAAAAQqI/X0-DR3_tM1U/photo.jpg?sz=50",
  "isDefault": false
 },
 "isPlusUser": true,
 "language": "en_GB",
 "ageRange": {
  "min": 21
 },
 "circledByCount": 181,
 "verified": false,
 "cover": {
  "layout": "banner",
  "coverPhoto": {
   "url": "https://lh3.googleusercontent.com/-27HUAD5FPvQ/T9kJ5W15cWI/AAAAAAAAAFo/vzdYiW-_Llc/s630-fcrop64=1,20302ec0df80d0ca/Colors%2Bof%2BAutumn.jpg",
   "height": 629,
   "width": 940
  }
 }
}*/
