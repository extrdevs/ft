var mongoose = require("mongoose");
var User = mongoose.model("User");
var NewUser = mongoose.model("NewUser");

var rek = require("rekuire");
var logger = rek("logger");

var LocalStrategy = require("passport-local").Strategy;

module.exports = new LocalStrategy(function (email, pwd, done) {
    logger.debug(email);
    User.findOne({
        email: email.toLowerCase()
    }).exec(function (err, user) {
        if (err) {
            return done(err);
        }
        if (!user) {
            return done(null, false, {
                message: "Incorrect username/password Initial."
            });
        }
        user.isValidPassword(pwd, function (err, result) {
            if (result === false) {
                //If password is wrong then it might have chnaged
                NewUser.findOne({
                    email: email.toLowerCase()
                }).exec(function (err, newUser) {
                    if (err) {
                        return done(err);
                    }
                    if (!newUser) {
                        return done(null, false, {
                            message: "No New users found "
                        });
                    }
                    newUser.isValidPassword(pwd, function (err, result) {
                        if (result === false) {
                            return done(null, false, {
                                message: "Incorrect username/password for New User"
                            });
                        } else {
                            var error = null;
                            if (NewUser.isValidToken(newUser.token)) {
                                user.pwd = newUser.pwd; //Updating password
                                user.save();
                            } else {
                                error = new Error("Invalid token/ Toke is expired")
                                user = null;
                            }
                            NewUser.remove({
                                email: email.toLowerCase()
                            }, function (err) {
                                if (err) return;
                                logger.debug("User " + user.email + " successfuly removed from newUser collection");
                            });
                            return done(error, user);
                        }
                    });
                });
            } else {
                return done(null, user);
            }
        });
    });
});
