var mongoose = require("mongoose");
var User = mongoose.model("User");

var BearerStrategy = require("passport-http-bearer").Strategy;
var jwt = require("jwt-simple");

//TODO token secret should not be visible to bare eyes
var tokenSecret = "xayzbcdefglkoqrsstyz";


module.exports = new BearerStrategy(function (token, done) {
    try {
        var decoded = jwt.decode(token, tokenSecret);

        //TODO: check token expiry here

        User.find({
            email: decoded.username
        }).exec(function (err, user) {
            if (err) {
                return done(err);
            }
            if (!user[0]) {
                return done(null, false); //no such user
            } else {
                return done(null, user[0]);
            }
        });
    } catch (err) {
        return done(null, false); //returns a 401 to the caller
    }
})
