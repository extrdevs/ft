var rek = require("rekuire");
var logger = rek("logger");
var passport = require("passport");

var authRoute = rek("authRoute");
var postRoute = rek("postRoute");
var userRoute = rek("userRoute");
var topicRoute = rek("topicRoute");
var ratingsRoute = rek("ratingsRoute");
var feedbackRoute = rek("feedbackRoute");
var routes = require("../routes");

var express = require("express");
var router = express.Router();
/**
 * Declare resource for routing.
 * @param  app - Express app
 */
module.exports = function(app) {
    router.use(function(req, res, next) {
    logger.debug("%s %s %s", req.method, req.url, req.path);
    next();
    });
/*************************** Authentication *****************************/
    var authRouter = express.Router();
    app.use("/auth", authRouter);
    //Login
    authRouter.post("/login", authRoute.login);
    // GET /auth/google
    //   Use passport.authenticate() as route middleware to authenticate the
    //   request.  The first step in Google authentication will involve
    //   redirecting the user to google.com.  After authorization, Google
    //   will redirect the user back to this application at /auth/google/callback
    authRouter.get("/google", passport.authenticate("google", {
        scope: ["https://www.googleapis.com/auth/plus.login",
  	"https://www.googleapis.com/auth/userinfo.email"]
    }));
    authRouter.get("/google/callback", authRoute.googleCallback);
    // GET /auth/facebook
    //   Use passport.authenticate() as route middleware to authenticate the
    //   request.  The first step in Facebook authentication will involve
    //   redirecting the user to facebook.com.  After authorization, Facebook
    //   will redirect the user back to this application at /auth/google/callback
    authRouter.get("/facebook", passport.authenticate("facebook", {
        scope: "public_profile,email"
    }));
    authRouter.get("/facebook/callback", authRoute.facebookCallback);
    //Logout
    authRouter.post("/logout", passport.authenticate("bearer", { session: false }), authRoute.logout);

/*************************** Place *****************************/
/* var placeRouter = express.Router();
    app.use("/place", placeRouter);
    placeRouter.post("/", passport.authenticate("bearer", {
        session: false
    }), placeRoute.addPlace);
    placeRouter.get("/", placeRoute.getAllPlaces);
    //placeRouter.get("/suggest", placeRoute.getSuggestedPlaces);
    placeRouter.get("/nearby", placeRoute.getNearbyPlaces);
    placeRouter.get("/types", placeRoute.getPlaceTypes);
    placeRouter.get("/cities", placeRoute.getCities);
    placeRouter.get("/areas", placeRoute.getAreas);
*/
/*************************** Post *****************************/
    var postRouter = express.Router();
    app.use("/post", postRouter);

    //Get posts
    postRouter.get("/", postRoute.getPosts);
    //Get a post with given id.
    postRouter.get("/:postId", postRoute.getPost);

    //Add post
    /* Creating limiter for restricting user to send too many post at a time
        https://www.npmjs.com/package/express-limiter#examples
    */
    var redisConnection = rek("redis-connection");

    redisConnection(function(redisClient){
        var limiter = require("express-limiter")(app, redisClient);
        postRouter.post("/", passport.authenticate("bearer", {session: false}),
                    limiter({
                      lookup: ["connection.remoteAddress", "user.id"],
                      //requests per hour
                      total: 50,
                      expire: 1000 * 60 * 60
                    }), postRoute.addPost);
    });

    //like api
    postRouter.put("/like", passport.authenticate("bearer", {session: false}), postRoute.likePost);
    //Like a comments
    postRouter.put("/likeComment", passport.authenticate("bearer", {session: false}), postRoute.likeComment);
    //Add comment
    postRouter.post("/:postId/com", passport.authenticate("bearer", {session: false}), postRoute.addComment);
    //Get Comments for post
    postRouter.get("/:postId/com", postRoute.getComments);
    //Like a comment from a post
    postRouter.put("/:postId/:comId/like", passport.authenticate("bearer", {session: false}),       postRoute.likeUnlikeComment);
    //Check if post has like
    postRouter.get("/:itemId/isLiked", passport.authenticate("bearer", {session: false}), postRoute.isLike);

/*************************** User *****************************/
    var userRouter = express.Router();
    app.use("/user", userRouter);
    //Add user
    userRouter.post("/", userRoute.addUser);
    //Verify added new user
    userRouter.get("/verify/:token", userRoute.verifyNewUserToken);
    //Verify forgot password
    userRouter.get("/fp/verify", userRoute.forgotPasswordVerify);
    //forgot password
    userRouter.post("/fp", userRoute.forgotPasswordRequest);
    //reset forgot password
    userRouter.post("/fp/reset", userRoute.forgotPasswordPerformReset);
    //Update user profile
    userRouter.put("/", passport.authenticate("bearer", {session: false}), userRoute.updateProfile);
    //Get all users for testing
    userRouter.get("/", userRoute.getAllUsers);
    //Get all user details for testing
    userRouter.get("/userDetails", userRoute.getAllUsersDetails);
    //Register client token with user.
    userRouter.post("/registerClient", passport.authenticate("bearer", {session: false}), userRoute.registerClient);

/*************************** Topic *****************************/
    var topicRouter = express.Router();
    app.use("/topic", topicRouter);
    //Create a new topic for discussion
    topicRouter.post("/", passport.authenticate("bearer", { session: false }), topicRoute.createTopic);

/*************************** Rating *****************************/
    var ratingsRouter = express.Router();
    app.use("/rating", ratingsRouter);
    //Get rating of a post
    ratingsRouter.post("/", passport.authenticate("bearer", {session: false}), ratingsRoute.getPostRating);
    //Update posts rating
    ratingsRouter.post("/update", passport.authenticate("bearer", {session: false}), ratingsRoute.updatePostRating);
    //Whats the average rating of the post
    ratingsRouter.get("/avg", ratingsRoute.getAveragePostRating);

/*************************** Feedback *****************************/
    var feedbackRouter = express.Router();
    app.use("/feedback", feedbackRouter);
    //Add feedfack
    feedbackRouter.post("/", feedbackRoute.saveFeedback);
/*************************** Server partial (pages) *****************************/
    app.get("/partials/:filename", routes.partials);
    app.use(routes.index);
};
