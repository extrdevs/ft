"use strict";

module.exports = {
    DB: {
        MONGO: {
            USERNAME: process.env.MONGO_USERS_NAME || "",
            PASSWORD: process.env.MONGO_PASSWORD || "",
            HOST: process.env.MONGO_HOST || "localhost",
            PORT: process.env.MONGO_PORT || "27017",
            DB_NAME: process.env.MONGO_DB || "mydb",
            IS_REMOVE_AUTO_INC_COUNTER: false
        },

        REDIS: {
            HOST: process.env.REDIS_HOST || "localhost",
            PORT: process.env.REDIS_PORT || 6379,
            PASSWORD: process.env.REDIS_PASSWORD || null,
            COMPLETER_NAMESPACE: "ft"
        },

        COLLECTIONS: {
            AREAS: "areas",
            COMMENTS: "comments",
            PLACES: "places",
            POSTS: "posts",
            USER_DETAILS: "userdetails",
            USERS: "users"
        }
    },

    APP: {
        TITLE: "PlaceCus WebApp - Development Environment",
    },

    APP_SERVER: {
        PORT: process.env.APP_PORT || 8080,
        HOST: process.env.APP_HOST || "",
        HOST_NAME: process.env.APP_HOST_NAME || "api.placetalkr.com"
    }
};
