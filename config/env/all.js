"use strict";

module.exports = {
    CORS: {
        ALLOWED_DOMAINS: "*",
        ALLOWED_METHODS: "GET, PUT, POST, DELETE, OPTIONS",
        ALLOWED_HEADERS: "x-requested-with, content-type, accept, origin, authorization, x-csrftoken"
    },
    GCM_API_KEY: "AIzaSyBC6p51CqRm9lMzv9zHimbw_2wMNkwcktU",
    PROTOCOL: "http"
};
