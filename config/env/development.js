"use strict";

module.exports = {
    DB: {
        MONGO: {
            HOST: process.env.MONGO_HOST || "127.0.0.1",
            PORT: process.env.MONGO_PORT || "27017",
            DB_NAME: process.env.MONGO_DB || "mydb",
            IS_REMOVE_AUTO_INC_COUNTER: false
        },

        REDIS: {
            HOST: process.env.REDIS_HOST || "localhost",
            PORT: process.env.REDIS_PORT || 6379,
            PASSWORD: process.env.REDIS_PASSWORD || "",
            COMPLETER_NAMESPACE: "ft"
        },

        COLLECTIONS: {
            AREAS: "areas",
            COMMENTS: "comments",
            PLACES: "places",
            POSTS: "posts",
            USER_DETAILS: "userdetails",
            USERS: "users"
        }
    },

    APP: {
        TITLE: "PlaceTalkr WebApp - Development Environment",
    },

    APP_SERVER: {
        PORT: process.env.APP_PORT || 3000,
        HOST: process.env.APP_HOST || "",
        HOST_NAME: process.env.APP_HOST_NAME || "localhost"
    }
};
