"use strict";

module.exports = {
    DB: {
        MONGO: {
                HOST: "www.placetalkr.com",
                DB_NAME: "mydb",
                IS_REMOVE_AUTO_INC_COUNTER: false
            },

        REDIS: {
                HOST: "www.placetalkr.com",
                PORT: 6379,
                COMPLETER_NAMESPACE: "ft"
            },

        COLLECTIONS: {
                AREAS: "areas",
                COMMENTS: "comments",
                PLACES: "places",
                POSTS: "posts",
                USER_DETAILS: "userdetails",
                USERS: "users"
            }
    },
    APP: {
        TITLE: "PlaceTalkr WebApp - Development Environment",
    },

    APP_SERVER: {
        PORT: 3000,
        ADDRESS: "127.0.0.1"
    }
};
