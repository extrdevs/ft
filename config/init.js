"use strict";

var rek = require("rekuire");
var logger = rek("logger");
var glob = require("glob");
var openShift = require("./platform/openshift-config");
var heroku = require("./platform/heroku-config");

/**
 * Module init function.
 */
module.exports = function() {
    /**
     * Before we begin, lets set the environment variable
     * We"ll Look for a valid NODE_ENV variable and if one cannot be found load the development NODE_ENV
     */
    var environmentFile = glob.sync("./config/env/" + process.env.NODE_ENV + ".js");
    if (!environmentFile.length) {
        if (process.env.NODE_ENV) {
            logger.debug("[Bootstrap] No configuration file found for " + process.env.NODE_ENV + "environment using development instead");
        } else {
            logger.debug("[Bootstrap] NODE_ENV is not defined! Using default development environment");
        }

        process.env.NODE_ENV = "development";
    } else {
        logger.info(" [Bootstrap] Application loaded using the " + process.env.NODE_ENV + " environment configuration");
    }

    logger.info(" [Bootstrap] process.env.OPENSHIFT_APP_DNS: " + process.env.OPENSHIFT_APP_DNS);

    if (process.env.OPENSHIFT_APP_DNS) {
        logger.info(" [Bootstrap] Reading openshift env variables");
        openShift();
    } else if (process.env.HEROKU) {
        logger.info(" [Bootstrap] Reading Heroku env variables");
        heroku();
    }
};
