var rek = require("rekuire");
var mongoconn = rek("mongo-connection");
//var redisconn = rek("redis-connection");
var fs = require("fs");
var path = require("path");
var em = rek("event-manager");
var logger = rek("logger");

module.exports = {

    init: function() {
            //Connect to MongoDB
            mongoconn();
            // Register for mongoose models
            fs.readdirSync(path.join(__dirname, "../models")).forEach(function(file) {
                if (~file.indexOf(".js")) require(path.join(__dirname, "../models", file));
            });

            em.ee.on("mongo-all-done", function() {
                logger.info(" [Bootstrap] Mongo init done");
                em.ee.emit("datastore-all-done");
            });
        }
};
