# Getting Started #

* **Clone Repo locally:**
```
#!javascript

git clone https://extrdevs@bitbucket.org/extrdevs/ft.git
```

* **Check version of the node**
Check version of the node in package.json and make sure you have that version of node and npm installed. Version other than package.jason could cause error while installation or while running application.

* **Run npm install**

```
#!javascript

npm intall  

```
* **Run the app using following command.**

```
#!javascript

node app.js
```




-------------------------------------------

* Manually copy and paste angular.cloudinary (snapshot) from github.
This is required to get fix which is not available with latest version of cloudinary_ng