'use strict';

module.exports = function InsufficientDataError(message, status) {
  Error.captureStackTrace(this, this.constructor);
  this.name = this.constructor.name;
  this.message = message || "Insufficient data to process request";
  this.errorCode = 12;
  this.status = status || 400;
};

Error.prototype.toString = function(){
    return JSON.stringify({
        "name" : this.name,
        "message": this.message,
        "errorCode": this.errorCode
    });
};

require('util').inherits(module.exports, Error);


/*It's nice to be able to distinguish error types by classes. But it's a bit tricky to correctly create a custom error class in Node.js, so here is an example.

The example also shows how to add an extra parameter called extra that will be stored as a property on the error.

###Usage

var CustomError = require('./errors/custom-error');

function doSomethingBad() {
  throw new CustomError('It went bad!', 42);
}

###Features

*Name appears once - less editing if you have to create lots of custom error classes
*Easy to subclass - just change the last line to inherit from another custom error class you created
*Correct stack trace - no extra stack frames, no double capturing of the stack trace

###Anti-patterns

Follofowing are some things that I've seen in other proposed solutions that you should avoid.

Error.call(this) - creates another error object (wasting a bunch of time) and doesn't touch this at all
Error.captureStackTrace(this, arguments.callee); - works, but arguments.callee is deprecated, so don't use it
this.stack = (new Error).stack - this... I don't even...*/
