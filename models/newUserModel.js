var mongoose = require("mongoose");
/*var autoIncrement = require("mongoose-auto-increment");*/
var bcrypt = require("bcrypt-nodejs");

//for generating token
var jwt = require("jsonwebtoken");
var privateKey = "newuserprivate";

//TODO refactor User to UserSchema
//TODO this is basic User schema, idea is to have light weight user schema
//for basic user specific manipulation.
//while another schema would required to capture advance user details like address, phone etc.

var NewUser = mongoose.Schema({
    name: String,
    email: {
        type: String,
        unique: true
    },
    pwd: String,
    token: String
});

NewUser.methods.isValidPassword = function isValidPassword(pwd, cb) {

    bcrypt.compare(pwd, this.pwd, function(err, result) {
        if (err) {
            return cb(err);
        } else {
            cb(err, result);
        }
    });
};

NewUser.statics.hash = function hash(text, cb) {
    bcrypt.genSalt(10, function(err, salt) {
        bcrypt.hash(text, salt, null, function(err, hash) {
            if (err) {
                return cb(err);
            } else {
                cb(err, hash);
            }
        });
    });
};

NewUser.methods.setToken = function setToken() {
    var token = jwt.sign({
        name: this.name,
        email: this.email
    }, privateKey, {
        expiresInMinutes: "10080"
    }); //i.e. 1 week
    this.token = token;
};

/*
Verify the token
 */
NewUser.statics.isValidToken = function isValidToken(token) {
    var isValid = true;
    try {
        jwt.verify(token, privateKey);
    } catch (ex) {
        isValid = false;
    }
    return isValid;
};



/*User.plugin(autoIncrement.plugin, "User");*/

var NewUserModel = mongoose.model("NewUser", NewUser);

module.exports = NewUserModel;
