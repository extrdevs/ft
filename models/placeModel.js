var mongoose = require("mongoose");


var Place = mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    name: String,
    type: Number,
    country: String,
    state: String,
    city: String,
    street: String,
    area1: String,
    area2: String,
    area3: String,
    area4: String,
    area5: String,
    area6: String,
    zip: String, //postal code
    lng: String,
    lat: String,
    phone: [{
        type: String
    }],
    attributes: String,
    createDate: {
        type: Date,
        default: Date.now
    },
    status: String,
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    image: {
        imgId: String,
        imgVer: String
    },
    //*version*
    //cloundry adds the image version in order to
    //make sure you get the new version and not a cached copy of the old one

    //rank is a number caclulated using likes, ccount, spam and other factors.
    //Idea is to retreive items with descending order of rank.
    rank: Number,
    lastUpdate: {
        type: Date,
        default: Date.now
    },
    //items avialable in given place
    items: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Post"
    }]
});

var PlaceModel = mongoose.model("Place", Place);

module.exports = PlaceModel;
