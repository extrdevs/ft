var mongoose = require("mongoose");
var createdModifiedPlugin = require("mongoose-createdmodified").createdModifiedPlugin;


var UserDetail = mongoose.Schema({
    name: String,
    secondaryId: {
        type: String,
        trim: true,
        index: true
    },
    //Posts Posted by users
    //see if can use hashset instead of list (mainly when have posts in 1000"s)
    PostedPosts: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Post"
    }],
    //liked Posts
    //see if can use hashset instead of list (mainly when have posts in 1000"s)
    likedPosts: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Post"
    }],
    //liked comments
    //see if can use hashset instead of list (mainly when have comments in 1000"s)
    likedComments: [{
        postId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Post"
        },
        comId: Number
    }],
    //ratings given by users
    ratings: [{
        postId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Post"
        },
        rating: Number
    }],
    helpful: Number,
    spam: Number,
    clientIds: [String]
});

//UserDetail.set("autoIndex", false);
UserDetail.plugin(createdModifiedPlugin, {
    index: false
});

var UserDetailModel = mongoose.model("UserDetail", UserDetail);

module.exports = UserDetailModel;
