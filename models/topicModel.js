var mongoose = require("mongoose");
//var UserModel = require("../models/userModel.js");

//TODO see if we can rename Topic to Item to make it more generic
var Topic = mongoose.Schema({
    //unique indexes on name
    name: { type: String, unique: true },
    //private or public sope
    scope: Number,
    //its context for the topic e.g.
    tag: String,
    //hashtags for particular topic
    tags: String,
    info: String,
    author: {type: mongoose.Schema.Types.ObjectId, ref: "User"},
    //rank is a number caclulated using likes, ccount, spam and other factors.
    //Idea is to retreive items with descending order of rank.
    rank: Number,
    //followers count
    followers: Number,
    image: {
        imgId: String,
        //*version*
        //cloundry adds the image version in order to
        //make sure you get the new version and not a cached copy of the old one
        imgVer: String
    },
    spam: Number,
    timestamp: { type: Date, default:Date.now}
});

var TopicModel = mongoose.model("Topic", Topic);

module.exports = TopicModel;
