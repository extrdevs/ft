var mongoose = require("mongoose");
var UserModel = require("../models/userModel.js");
var TopicModel = require("../models/topicModel.js");

//TODO see if we can rename Topic to Item to make it more generic
var TopicMeta = mongoose.Schema({

    topic: {type: mongoose.Schema.Types.ObjectId, ref: "Topic"},
    //following users
    followers: [{type: mongoose.Schema.Types.ObjectId, ref: "User"}]
});

var TopicMetaModel = mongoose.model("TopicMeta", TopicMeta);

module.exports = TopicMetaModel;
