var mongoose = require("mongoose");
var createdModifiedPlugin = require("mongoose-createdmodified").createdModifiedPlugin;
//var PlaceModel = require("../models/placeModel.js");
/*var UserModel = require("../models/userModel.js");
var TopicModel = require("../models/topicModel.js");*/

//TODO see if we can rename Post to Item to make it more generic
var Post = mongoose.Schema({
    //_id is autocreated by mongodb
	//_id            : mongoose.Schema.Types.ObjectId,
    title         : String,
    //description
    des           : String,
    type          : Number,
    area          : String,
    areaName      : String,
    attributes    : String,
    author        : { type : mongoose.Schema.Types.ObjectId, ref : "User"},
    image         : { imgId: String, imgVer: String},
        //*version*
        //cloundry adds the image version in order to
        //make sure you get the new version and not a cached copy of the old one

    //rank is a number caclulated using likes, ccount, spam and other factors.
    //Idea is to retreive items with descending order of rank.
    rank           : Number,
    comments       : [{ type: mongoose.Schema.Types.ObjectId, ref: "Comment"}],
    comCount       : {type: Number, default: 0},
    status         : String,
    likes          : Number,
    spam           : Number,
    likedBy        : [{ type : mongoose.Schema.Types.ObjectId, ref : "User"}],
    //option to be updated in api
    //ratings recieved by posts
    ratings        : [{
            userId   : { type: mongoose.Schema.Types.ObjectId, ref: "User"},
            rating   : Number
    }],

    //this is use to autoincreament comment id which goes into array
    //adding one to this field will be unique comment id for next comment
    comLastId: {type: Number, default: 0}
});

//Operations before saving the post
Post.pre("save", function(next) {
  this.rank = getCalculateRank(this.likes, this.spam, this.created);
  next();
});

/**
 * Calculate the rank of the post which will determine value of the post.
 * Current number of second and the likes, spams decide the rank.
 * Default setting for the rank is current Epoch time. (number of seconds elapsed
 * since 00:00:00 Coordinated Universal Time (UTC), Thursday, 1 January 1970)
 * @returns rank
 */
var getCalculateRank = function(likes, spams, created){
    likes = (likes) ? likes : 0;
    spams = (spams) ? spams : 0;
    var rank = 0;

    if(created){
       // console.log(created);
        rank = created.getTime() + (likes * 12 * 60 * 60 * 1000)
            - (spams * 12 * 60 * 60 * 1000); // One like/spam equals 12 Hr as of now
    } else {
       // console.log("No date");
        rank = new Date().getTime() + (likes * 12 * 60 * 60 * 1000)
            - (spams * 12 * 60 * 60 * 1000);
    }
    return rank;
};

Post.plugin(createdModifiedPlugin, {index: true});

var PostModel = mongoose.model("Post", Post);

module.exports = PostModel;
