var mongoose = require("mongoose");
//var UserModel = require("../models/userModel.js");
//var TopicModel = require("../models/topicModel.js");
/*var autoIncrement = require("mongoose-auto-increment");*/
var bcrypt = require("bcrypt-nodejs");

//TODO refactor UserMeta to UserMetaSchema
//TODO this is basic UserMeta schema, idea is to have light weight UserMeta schema
//for basic UserMeta specific manipulation.
//while another schema would required to capture advance UserMeta details like address, phone etc.

var UserMeta = mongoose.Schema({
	//liked Posts
	posts: [{type: mongoose.Schema.Types.ObjectId, ref: "Post"}],
	//following topics
	topics: [{type: mongoose.Schema.Types.ObjectId, ref: "Topic"}],
	//not required directly but may be useful in catching.
	user: {type: mongoose.Schema.Types.ObjectId, ref: "User"}
});

UserMeta.methods.isValidPassword = function isValidPassword(pwd, cb) {

	bcrypt.compare(pwd, this.pwd, function(err, result) {
		if(err){
			return cb(err);
		}else{
			cb(err, result);
		}
	});
};

UserMeta.statics.hash = function hash(text, cb) {
	bcrypt.genSalt(10, function(err, salt) {
		bcrypt.hash(text, salt, null, function(err, hash) {

			if(err){
				return cb(err);
			}else{
				cb(err, hash);
			}
		});
	});
};

/*UserMeta.plugin(autoIncrement.plugin, "UserMeta");*/

var UserMetaModel = mongoose.model("UserMeta", UserMeta);

module.exports = UserMetaModel;
