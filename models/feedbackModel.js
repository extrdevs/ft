var mongoose = require("mongoose");


var Feedback = mongoose.Schema({
    author      : { type : mongoose.Schema.Types.ObjectId, ref : "User"},
    comments    : String,
    rating      : { type: Number, min: 1, max: 5},
    email       : String
});

var feedbackModel = mongoose.model("Feedback", Feedback);

module.exports = feedbackModel;
