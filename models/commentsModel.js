/*TODO 1 before deployment into production, study indexing, slug
and see how it can be applied on models we are persisting here.
There shouldn"t be any modification into model after production deployment*/
var mongoose = require("mongoose");
//var UserModel = require("../models/userModel.js");
//var PostModel = require("../models/postModel.js");
var createdModifiedPlugin = require("mongoose-createdmodified").createdModifiedPlugin;

var Comments = mongoose.Schema({

    postId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Post"
    },
    page: {
        type: Number,
        default: 1
    },
    count: {
        type: Number,
        default: 1
    },
    comments: [{
        _id: Number,
        details: String,
        author: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User"
        },
        //mostly not required for comment but lets have it incase required.
        rank: Number,
        likedBy: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: "User"
        }],
        likes: {type: Number, default: 0},
        spam: {type: Number, default: 0},
        created: {
            type: Date,
            default: Date.now
        },
        modified: {
            type: Date,
            default: Date.now
        }
    }]
});

Comments.plugin(createdModifiedPlugin, {
    index: false
});

var CommentsModel = mongoose.model("Comments", Comments);

module.exports = CommentsModel;
