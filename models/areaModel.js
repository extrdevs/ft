
var mongoose = require("mongoose");
var createdModifiedPlugin = require("mongoose-createdmodified").createdModifiedPlugin;
//var autoIncrement = require("mongoose-auto-increment");

//Difference between placeModel & areaModel
//placeModel -> single point on geographical surface e.g. restaurent
//areaModel -> area having perimeter on a geographical surface - e.g. pune
var area = mongoose.Schema({
    name: String,
    short_name: String,
    type: Number,
    city: String,
    state: String,
    region: String,
    country: String,
    zip: String,
    lng: String,
    lat: String,
    vicinity: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Area"
    }],
    img: {
        img_id: String,
        img_ver: String
    },
    rank: Number
});

/*area.plugin(autoIncrement.plugin, {
    model: "Area",
    startAt: 1,
});*/

area.plugin(createdModifiedPlugin, {index: false});
//area.plugin(autoIncrement.plugin, "Area");

var AreaModel = mongoose.model("Area", area);

module.exports = AreaModel;
