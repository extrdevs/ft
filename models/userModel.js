var mongoose = require("mongoose");
/*var autoIncrement = require("mongoose-auto-increment");*/
var bcrypt = require("bcrypt-nodejs");
var createdModifiedPlugin = require("mongoose-createdmodified").createdModifiedPlugin;


//TODO refactor User to UserSchema
//TODO this is basic User schema, idea is to have light weight user schema
//for basic user specific manipulation.
//while another schema would required to capture advance user details like address, phone etc.

var User = mongoose.Schema({
    name: String,
    email: {
        type: String,
        unique: true
    },
    pwd: String,
    image: {
        imgId: String,
        //*version*
        //cloundry adds the image version in order to
        //make sure you get the new version and not a cached copy of the old one
        imgVer: String
    },
    userName: String,
    provider: String,
    accessToken: String,
		verifierToken: String,
		verifierTokenTimestamp: {type: Date}
});

User.methods.isValidPassword = function isValidPassword(pwd, cb) {

    bcrypt.compare(pwd, this.pwd, function(err, result) {
        if (err) {
            return cb(err);
        } else {
            cb(err, result);
        }
    });
};

User.statics.hash = function hash(text, cb) {
    bcrypt.genSalt(10, function(err, salt) {
        bcrypt.hash(text, salt, null, function(err, hash) {
            if (err) {
                return cb(err);
            } else {
                cb(err, hash);
            }
        });
    });
};

/*User.plugin(autoIncrement.plugin, "User");*/

User.plugin(createdModifiedPlugin, {
    index: false
});

var UserModel = mongoose.model("User", User);

module.exports = UserModel;
